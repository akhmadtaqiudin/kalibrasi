<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/pages/template/include.jsp"></jsp:include>
<title>Kalibrasi</title>
<script type="text/javascript">
	$(window).ready(function(){
		$("#la").change(function(){
			var varid = $("#la").val();
			if(varid==""){
				$("#ida").val("");
			}
			if(varid!=""){
				var parent = "${pageContext.request.contextPath}";
				$.ajax({
				   url: parent+"/alat-ukur/SelectByJson.action?alatUkur.namaAlat="+varid,
				   type: 'POST',
		           dataType: 'json',
		           success:  function(responseDariServer){
					   $("#ida").val(responseDariServer.kodeAlat);
				   },
				   error: function(req, status, err){
					alert("gagal melakukan koneksi data alat ukur")
				   }
				});
			}
		});
	
		if($("#la").val()==""){
			$("#ida").val("");
		}else{
			var varid = $("#la").val();
			var parent = "${pageContext.request.contextPath}";
				$.ajax({
				   url: parent+"/alat-ukur/SelectByJson.action?alatUkur.namaAlat="+varid,
				   success:  function(responseDariServer){
					$("#ida").val(responseDariServer.kodeAlat);
				   },
				   error: function(req, status, err){
					alert("gagal mengambil data dari tabel alat ukur")
				   }
			});
		}
		
		$("#ld").change(function(){
			var varid = $("#ld").val();
			if(varid==""){
				$("#idd").val("");
			}
			if(varid!=""){
				var parent = "${pageContext.request.contextPath}";
				$.ajax({
				   url: parent+"/departement/SelectByJson.action?departement.namaDept="+varid,
				   type: 'POST',
		           dataType: 'json',
		           success:  function(responseDariServer){
					   $("#idd").val(responseDariServer.idDept);
				   },
				   error: function(req, status, err){
					alert("gagal melakukan koneksi data departement")
				   }
				});
			}
		});
	
		if($("#ld").val()==""){
			$("#idd").val("");
		}else{
			var varid = $("#ld").val();
			var parent = "${pageContext.request.contextPath}";
				$.ajax({
				   url: parent+"/departement/SelectByJson.action?departement.namaDept="+varid,
				   success:  function(responseDariServer){
					$("#idd").val(responseDariServer.idDept);
				   },
				   error: function(req, status, err){
					alert("gagal mengambil data dari tabel departement")
				   }
			});
		}
		
		$("#lk").change(function(){
			var varid = $("#lk").val();
			if(varid==""){
				$("#idk").val("");
			}
			if(varid!=""){
				var parent = "${pageContext.request.contextPath}";
				$.ajax({
				   url: parent+"/kalibrator/SelectByJson.action?kalibrator.namaKalibrator="+varid,
				   type: 'POST',
		           dataType: 'json',
		           success:  function(responseDariServer){
					   $("#idk").val(responseDariServer.kodeKalibrator);
				   },
				   error: function(req, status, err){
					alert("gagal melakukan koneksi data kalibrator")
				   }
				});
			}
		});
	
		if($("#lk").val()==""){
			$("#idk").val("");
		}else{
			var varid = $("#lk").val();
			var parent = "${pageContext.request.contextPath}";
				$.ajax({
				   url: parent+"/kalibrator/SelectByJson.action?kalibrator.namaKalibrator="+varid,
				   success:  function(responseDariServer){
					$("#idk").val(responseDariServer.kodeKalibrator);
				   },
				   error: function(req, status, err){
					alert("gagal mengambil data dari tabel kalibrator")
				   }
			});
		}
		
		/*start dynamic field By Muhammad Huzaifah 14 Oktober 2016*/
		var max_fields      = 20; //maximum input boxes allowed
	    var wrapper         = $("#input_fields_wrap"); //Fields wrapper
	    var add_button      = $("#add_field_button"); //Add button ID
	    
	    var x = $(".counter").length; //initlal text box count
	    $(add_button).on("click",function(){ //on add input button click
	        if(x < max_fields){ //max input box allowed
	            x++; //text box increment

	            $(wrapper).append('<div class="counter">'
	            		+'<div class="control-group">'
	            		+'<div class="controls">'
	            		+'<s:textfield name="hasilUkur1" theme="simple" placeholder="hasilUkur 1" />'
	            		+'<s:textfield name="hasilUkur2" theme="simple" placeholder="hasilUkur 2" />'
	            		+'<s:textfield name="hasilUkur3" theme="simple" placeholder="hasilUkur 3" />'
	            		+'<s:textfield name="hasilUkur4" theme="simple" placeholder="hasilUkur 4" />'
	            		+'<s:textfield name="hasilUkur5" theme="simple" placeholder="hasilUkur 5" />'
	            		+'&nbsp;<input type="button" value="&nbsp;Hapus&nbsp;" class="remove_field btn btn-danger btn-sm">'
	            		+'</div>'
	            		+'</div>'
	            		+'</div>'); //add input box
	        }

	    });
	    
	    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
	        e.preventDefault(); $(this).parent('div').parent('div').parent('div').remove(); x--;
	    });
	});
</script>
</head>
<body>
	<div class="wrapper">
		<header class="main-header"> <!-- Logo -->
		 <a
			href="index2.html" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>D</b></span> <!-- logo for regular state and mobile devices -->
			<span class="logo-lg">Dashboard</span>
		</a> <!-- Header Navbar: style can be found in header.less --> <nav
			class="navbar navbar-static-top"> <!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <img
						src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg"
						class="user-image" alt="User Image"> <span class="hidden-xs"><s:property
								value="#session['userName'].userName" /></span>
				</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header"><img
							src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg"
							class="img-circle" alt="User Image"></li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a
									href="${pageContext.request.contextPath}/authentication/logout.action"
									class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul></li>
			</ul>
		</div>
		</nav> </header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
	    <!-- sidebar: style can be found in sidebar.less -->
	    <section class="sidebar">
	      <!-- Sidebar user panel -->
	      <div class="user-panel">
	        <div class="pull-left image">
	          <img src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
	        </div>
	        <div class="pull-left info">
	          <p id="nama"><s:property value="#session['userName'].userName" /></p>
	        </div>
	      </div>
	      <!-- /.search form -->
	      <!-- sidebar menu: : style can be found in sidebar.less -->
	      <ul class="sidebar-menu" data-widget="tree">
	        <li class="header">MAIN NAVIGATION</li>
	       <li class="treeview">
	          <a href="#">
	            <i class="fa fa-dashboard"></i> <span>Master Data</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="${pageContext.request.contextPath}/alat-ukur/SearchAlatUkur.action"><i class="fa fa-cog"></i> Alat Ukur </a></li>
	            <li><a href="${pageContext.request.contextPath}/departement/SearchDepartement.action"><i class="fa fa-cubes"></i> Departement </a></li>
	            <li><a href="${pageContext.request.contextPath}/kalibrator/SearchKalibrator.action"><i class="fa fa-gears"></i> Kalibrator </a></li>
	          </ul>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/permohonan-kalibrasi/SearchPermohonan.action">
	            <i class="fa fa-file-text-o"></i> <span>Surat Permohonan</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/jadwal-kalibrasi/SearchJadwalKalibrasi.action">
	            <i class="fa fa-calendar"></i> <span>Jadwal Kalibrasi</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/rekalibrasi/SearchReKalibrasi.action">
	            <i class="fa fa-th"></i> <span>Re-Kalibrasi</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-files-o"></i>
	            <span>Laporan</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="${pageContext.request.contextPath}/rekalibrasi/ReportReKalibrasi.action"><i class="fa fa-circle-o"></i> Re-Kalibrasi </a></li>
	            <li><a href="${pageContext.request.contextPath}/permohonan-kalibrasi/ReportPermohonan.action"><i class="fa fa-file-text-o"></i>Hasil Kalibrasi </a></li>
	            <%-- <li><a href="${pageContext.request.contextPath}/jadwal-kalibrasi/ReportJadwalKalibrasi.action"><i class="fa fa-calendar"></i> Jadwal Kalibrasi </a></li> --%>
	          </ul>
	        </li>
	        <%-- <li>
	          <a href="${pageContext.request.contextPath}/authentication/allUser.action">
	            <i class="fa fa-users"></i> <span>Management User</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li> --%>
	      </ul>
	    </section>
	    <!-- /.sidebar -->
	  </aside>
		<div class="content-wrapper">
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Tambah Data Re-Kalibrasi</h3>
							</div>
							<s:action namespace="/alat-ukur" name="SelectAll" id="alat"/>
							<s:action namespace="/departement" name="SelectAll" id="dep"/>
							<s:action namespace="/kalibrator" name="SelectAll" id="kal"/>
							<s:form namespace="/rekalibrasi" method="POST" theme="bootstrap" cssClass="form-horizontal">
								<div class="box-body">
									<div class="form-group">
										<label class="col-sm-2 control-label">Jenis Rekalibrasi  :</label>	
										<div class="col-sm-10">
											<s:select list="#{'Vernier Caliper':'Vernier Caliper','Mikrometer':'Mikrometer','Profil Proyektor':'Profil Proyektor','Re-kal Dial':'Re-kal Dial'}" 
											headerKey="0" headerValue="==Pilih==" name="reKalibrasi.jenis" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">No Laporan  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.noLaporan" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kode Alat  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kodeAlat" cssClass="form-control" id="ida" readonly="true"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Nama Alat  :</label>	
										<div class="col-sm-10">
											<s:select id="la" list="#alat.listAlat" listKey="namaAlat" listValue="namaAlat" headerKey="0" 
												headerValue="==Pilih==" name="reKalibrasi.namaAlat" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kode Departemen  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.idDept" cssClass="form-control" id="idd" readonly="true"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Nama Departemen  :</label>	
										<div class="col-sm-10">
											<s:select id="ld" list="#dep.listDepartement" listKey="namaDept" listValue="namaDept" headerKey="0" 
												headerValue="==Pilih==" name="reKalibrasi.namaDept" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kode Kalibrator  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kodeKalibrator" cssClass="form-control" id="idk" readonly="true"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Nama Kalibrator  :</label>	
										<div class="col-sm-10">
											<s:select id="lk" list="#kal.listKalibrator" listKey="namaKalibrator" listValue="namaKalibrator" headerKey="0" 
												headerValue="==Pilih==" name="reKalibrasi.namaKalibrator" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Range Resolusi  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.rangeResolusi" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Penunjukan Kal  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.penunjukanKal" cssClass="form-control" />
										</div>
									</div>
									<div id="header_fields_wrap">
										<div class="field-content">Hasil Ukur 1</div>
										<div class="field-content">Hasil Ukur 2</div>
										<div class="field-content">Hasil Ukur 3</div>
										<div class="field-content">Hasil Ukur 4</div>
										<div class="field-right">Hasil Ukur 5</div>
									</div>
									<div id="input_fields_wrap">	
										<div class="counter">
											<div class="control-group">
												<div class="controls">
													<s:textfield name="hasilUkur1" theme="simple" placeholder="HasilUkur 1"/>
													<s:textfield name="hasilUkur2" theme="simple" placeholder="HasilUkur 2"/>
													<s:textfield name="hasilUkur3" theme="simple" placeholder="HasilUkur 3"/>
													<s:textfield name="hasilUkur4" theme="simple" placeholder="HasilUkur 4"/>
													<s:textfield name="hasilUkur5" theme="simple" placeholder="HasilUkur 5"/>
													<input type="button" value="Tambah " id="add_field_button" class="btn btn-success btn-sm form-control" />
												</div>
											</div>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">X Rata  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.rataRata" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Penyimpangan  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.penyimpangan" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kelembaban  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kelembaban" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Suhu  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.suhu" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Hasil  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.hasil" cssClass="form-control" />
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-2 control-label">Catatan  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.catatan" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Pelaksana Kal  :</label>
										<div class="col-sm-10">
											<s:select list="#{'Admin':'Admin','Qa':'Qa','User':'User'}" 
											headerKey="0" headerValue="==Pilih==" name="reKalibrasi.pelaksanaKal" cssClass="form-control" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kal Selanjutnya  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kalSelanjutnya" cssClass="form-control" />
										</div>
									</div>
								</div>
								
								<div class="box-footer">
									<s:submit action="SearchReKalibrasi" value="Cansel"	cssClass="btn btn-default pull-right" />
									<s:submit action="SaveReKalibrasi" value="Save"	cssClass="btn btn-primary pull-right" />
								</div>
							</s:form>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</body>
</html>