<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/pages/template/include.jsp"></jsp:include>
<title>Kalibrasi</title>
</head>
<body>
	<div class="wrapper">
		<header class="main-header"> <!-- Logo -->
		 <a
			href="index2.html" class="logo"> <!-- mini logo for sidebar mini 50x50 pixels -->
			<span class="logo-mini"><b>D</b></span> <!-- logo for regular state and mobile devices -->
			<span class="logo-lg">Dashboard</span>
		</a> <!-- Header Navbar: style can be found in header.less --> <nav
			class="navbar navbar-static-top"> <!-- Sidebar toggle button-->
		<a href="#" class="sidebar-toggle" data-toggle="push-menu"
			role="button"> <span class="sr-only">Toggle navigation</span>
		</a>
		<div class="navbar-custom-menu">
			<ul class="nav navbar-nav">
				<!-- User Account: style can be found in dropdown.less -->
				<li class="dropdown user user-menu"><a href="#"
					class="dropdown-toggle" data-toggle="dropdown"> <img
						src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg"
						class="user-image" alt="User Image"> <span class="hidden-xs"><s:property
								value="#session['userName'].userName" /></span>
				</a>
					<ul class="dropdown-menu">
						<!-- User image -->
						<li class="user-header"><img
							src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg"
							class="img-circle" alt="User Image"></li>
						<!-- Menu Footer-->
						<li class="user-footer">
							<div class="pull-left">
								<a href="#" class="btn btn-default btn-flat">Profile</a>
							</div>
							<div class="pull-right">
								<a
									href="${pageContext.request.contextPath}/authentication/logout.action"
									class="btn btn-default btn-flat">Sign out</a>
							</div>
						</li>
					</ul></li>
			</ul>
		</div>
		</nav> </header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
	    <!-- sidebar: style can be found in sidebar.less -->
	    <section class="sidebar">
	      <!-- Sidebar user panel -->
	      <div class="user-panel">
	        <div class="pull-left image">
	          <img src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
	        </div>
	        <div class="pull-left info">
	          <p id="nama"><s:property value="#session['userName'].userName" /></p>
	        </div>
	      </div>
	      <!-- /.search form -->
	      <!-- sidebar menu: : style can be found in sidebar.less -->
	      <ul class="sidebar-menu" data-widget="tree">
	        <li class="header">MAIN NAVIGATION</li>
	       <li class="treeview">
	          <a href="#">
	            <i class="fa fa-dashboard"></i> <span>Master Data</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="${pageContext.request.contextPath}/alat-ukur/SearchAlatUkur.action"><i class="fa fa-cog"></i> Alat Ukur </a></li>
	            <li><a href="${pageContext.request.contextPath}/departement/SearchDepartement.action"><i class="fa fa-cubes"></i> Departement </a></li>
	            <li><a href="${pageContext.request.contextPath}/kalibrator/SearchKalibrator.action"><i class="fa fa-gears"></i> Kalibrator </a></li>
	          </ul>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/permohonan-kalibrasi/SearchPermohonan.action">
	            <i class="fa fa-file-text-o"></i> <span>Surat Permohonan</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/jadwal-kalibrasi/SearchJadwalKalibrasi.action">
	            <i class="fa fa-calendar"></i> <span>Jadwal Kalibrasi</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/rekalibrasi/SearchReKalibrasi.action">
	            <i class="fa fa-th"></i> <span>Re-Kalibrasi</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-files-o"></i>
	            <span>Laporan</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="${pageContext.request.contextPath}/rekalibrasi/ReportReKalibrasi.action"><i class="fa fa-circle-o"></i> Re-Kalibrasi </a></li>
	            <li><a href="${pageContext.request.contextPath}/permohonan-kalibrasi/ReportPermohonan.action"><i class="fa fa-file-text-o"></i>Hasil Kalibrasi </a></li>
	            <%-- <li><a href="${pageContext.request.contextPath}/jadwal-kalibrasi/ReportJadwalKalibrasi.action"><i class="fa fa-calendar"></i> Jadwal Kalibrasi </a></li> --%>
	          </ul>
	        </li>
	        <%-- <li>
	          <a href="${pageContext.request.contextPath}/authentication/allUser.action">
	            <i class="fa fa-users"></i> <span>Management User</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li> --%>
	      </ul>
	    </section>
	    <!-- /.sidebar -->
	  </aside>
		<div class="content-wrapper">
			<section class="content">
				<div class="row">
					<div class="col-md-12">
						<div class="box box-info">
							<div class="box-header with-border">
								<h3 class="box-title">Detail Data Re-Kalibrasi</h3>
							</div>
							<s:action namespace="/alat-ukur" name="SelectAll" id="alat"/>
							<s:action namespace="/departement" name="SelectAll" id="dep"/>
							<s:action namespace="/kalibrator" name="SelectAll" id="kal"/>
							<s:form namespace="/rekalibrasi" method="POST" theme="bootstrap" cssClass="form-horizontal">
								<div class="box-body">
									<div class="form-group">
										<label class="col-sm-2 control-label">Kode Rekalibrasi  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.id" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Jenis Rekalibrasi  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.jenis" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Tanggal Kal  :</label>	
										<div class="col-sm-10">
											<s:textfield name="strDate" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">No Laporan  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.noLaporan" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kode Alat  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kodeAlat" cssClass="form-control" readonly="true"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Nama Alat  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.namaAlat" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kode Departemen  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.idDept" cssClass="form-control" readonly="true"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Nama Departemen  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.namaDept" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kode Kalibrator  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kodeKalibrator" cssClass="form-control" readonly="true"/>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Nama Kalibrator  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.namaKalibrator" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Range Resolusi  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.rangeResolusi" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Penunjukan Kal  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.penunjukanKal" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div id="header_fields_wrap">
										<div class="field-content">Hasil Ukur 1</div>
										<div class="field-content">Hasil Ukur 2</div>
										<div class="field-content">Hasil Ukur 3</div>
										<div class="field-content">Hasil Ukur 4</div>
										<div class="field-right">Hasil Ukur 5</div>
									</div>									
									<div id="input_fields_wrap">
										<s:iterator value="listDetailJenis">
											<div class="counter">
												<div class="control-group">
													<div class="controls">
														<s:textfield name="hasilUkur1" theme="simple" placeholder="HasilUkur 1" readonly="true"/>
														<s:textfield name="hasilUkur2" theme="simple" placeholder="HasilUkur 2" readonly="true"/>
														<s:textfield name="hasilUkur3" theme="simple" placeholder="HasilUkur 3" readonly="true"/>
														<s:textfield name="hasilUkur4" theme="simple" placeholder="HasilUkur 4" readonly="true"/>
														<s:textfield name="hasilUkur5" theme="simple" placeholder="HasilUkur 5" readonly="true"/>
													</div>
												</div>
											</div>
										</s:iterator>	
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">X Rata  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.rataRata" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Penyimpangan  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.penyimpangan" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kelembaban  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kelembaban" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Suhu  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.suhu" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Hasil  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.hasil" cssClass="form-control" readonly="true" />
										</div>
									</div>									
									<div class="form-group">
										<label class="col-sm-2 control-label">Catatan  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.catatan" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Pelaksana Kal  :</label>
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.pelaksanaKal" cssClass="form-control" readonly="true" />
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2 control-label">Kal Selanjutnya  :</label>	
										<div class="col-sm-10">
											<s:textfield name="reKalibrasi.kalSelanjutnya" cssClass="form-control" readonly="true" />
										</div>
									</div>
								</div>
								
								<div class="box-footer">
									<s:submit action="SearchReKalibrasi" value="Close"	cssClass="btn btn-default pull-right" />
									<s:submit action="DeleteReKalibrasi" value="Delete"	cssClass="btn btn-danger pull-right" />
								</div>
							</s:form>
						</div>
					</div>
				</div>
			</section>
		</div>
	</div>
</body>
</html>