<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib uri="http://displaytag.sf.net" prefix="display"%>
<%@page import="com.id.kalibrasi.modul.model.JadwalKalibrasi"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<jsp:include page="/pages/template/include.jsp"></jsp:include>
<title>Kalibrasi</title>
<script type="text/javascript">
		$(window).ready(function(){
			$(".del").click(function(){
			    if (!confirm("Apakah anda yakin ingin hapus file tersebut")){
			      return false;
			    }
			});
			$("#TablelistJadwal  > thead tr th").css({"text-align": "center", "vertical-align": "middle"});
			$("#TablelistJadwal  > tbody tr td").css({"text-align": "center", "vertical-align": "middle"});
		});
	</script>
</head>
<body>
	<div class="wrapper">
	  <header class="main-header">
	    <!-- Logo -->
	    <a href="index2.html" class="logo">
	      <!-- mini logo for sidebar mini 50x50 pixels -->
	      <span class="logo-mini"><b>D</b></span>
	      <!-- logo for regular state and mobile devices -->
	      <span class="logo-lg">Dashboard</span>
	    </a>
	    <!-- Header Navbar: style can be found in header.less -->
	    <nav class="navbar navbar-static-top">
	      <!-- Sidebar toggle button-->
	      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
	        <span class="sr-only">Toggle navigation</span>
	      </a>
	      <div class="navbar-custom-menu">
	        <ul class="nav navbar-nav">
	          <!-- User Account: style can be found in dropdown.less -->
	          <li class="dropdown user user-menu">
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <img src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg" class="user-image" alt="User Image">
	              <span class="hidden-xs"><s:property value="#session['userName'].userName" /></span>
	            </a>
	            <ul class="dropdown-menu">
	              <!-- User image -->
	              <li class="user-header">
	                <img src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
	              </li>
	              <!-- Menu Footer-->
	              <li class="user-footer">
	                <div class="pull-left">
	                  <a href="#" class="btn btn-default btn-flat">Profile</a>
	                </div>
	                <div class="pull-right">
	                  <a href="${pageContext.request.contextPath}/authentication/logout.action" class="btn btn-default btn-flat">Sign out</a>
	                </div>
	              </li>
	            </ul>
	          </li>
	        </ul>
	      </div>
	    </nav>
	  </header>
	  <!-- Left side column. contains the logo and sidebar -->
	  <aside class="main-sidebar">
	    <!-- sidebar: style can be found in sidebar.less -->
	    <section class="sidebar">
	      <!-- Sidebar user panel -->
	      <div class="user-panel">
	        <div class="pull-left image">
	          <img src="${pageContext.request.contextPath}/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
	        </div>
	        <div class="pull-left info">
	          <p id="nama"><s:property value="#session['userName'].userName" /></p>
	        </div>
	      </div>
	      <!-- /.search form -->
	      <!-- sidebar menu: : style can be found in sidebar.less -->
	      <ul class="sidebar-menu" data-widget="tree">
	        <li class="header">MAIN NAVIGATION</li>
	       <li class="treeview">
	          <a href="#">
	            <i class="fa fa-dashboard"></i> <span>Master Data</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="${pageContext.request.contextPath}/alat-ukur/SearchAlatUkur.action"><i class="fa fa-cog"></i> Alat Ukur </a></li>
	            <li><a href="${pageContext.request.contextPath}/departement/SearchDepartement.action"><i class="fa fa-cubes"></i> Departement </a></li>
	            <li><a href="${pageContext.request.contextPath}/kalibrator/SearchKalibrator.action"><i class="fa fa-gears"></i> Kalibrator </a></li>
	          </ul>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/permohonan-kalibrasi/SearchPermohonan.action">
	            <i class="fa fa-file-text-o"></i> <span>Surat Permohonan</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/jadwal-kalibrasi/SearchJadwalKalibrasi.action">
	            <i class="fa fa-calendar"></i> <span>Jadwal Kalibrasi</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li>
	          <a href="${pageContext.request.contextPath}/rekalibrasi/SearchReKalibrasi.action">
	            <i class="fa fa-th"></i> <span>Re-Kalibrasi</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li>
	        <li class="treeview">
	          <a href="#">
	            <i class="fa fa-files-o"></i>
	            <span>Laporan</span>
	            <span class="pull-right-container">
	              <i class="fa fa-angle-left pull-right"></i>
	            </span>
	          </a>
	          <ul class="treeview-menu">
	            <li><a href="${pageContext.request.contextPath}/rekalibrasi/ReportReKalibrasi.action"><i class="fa fa-circle-o"></i> Re-Kalibrasi </a></li>
	            <li><a href="${pageContext.request.contextPath}/permohonan-kalibrasi/ReportPermohonan.action"><i class="fa fa-file-text-o"></i>Hasil Kalibrasi </a></li>
	            <%-- <li><a href="${pageContext.request.contextPath}/jadwal-kalibrasi/ReportJadwalKalibrasi.action"><i class="fa fa-calendar"></i> Jadwal Kalibrasi </a></li> --%>
	          </ul>
	        </li>
	        <%-- <li>
	          <a href="${pageContext.request.contextPath}/authentication/allUser.action">
	            <i class="fa fa-users"></i> <span>Management User</span>
	            <span class="pull-right-container">
	              <!-- <small class="label pull-right bg-green">new</small> -->
	            </span>
	          </a>
	        </li> --%>
	      </ul>
	    </section>
	    <!-- /.sidebar -->
	  </aside>
	
	  <!-- Content Wrapper. Contains page content -->
	  <div class="content-wrapper">
		<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box box-primary">
					<div class="box-header">
						<h3 class="box-title">Master Jadwal Kalibrasi</h3>

						<div class="box-tools">
							<div class="input-group input-group-sm" style="width: 150px;">
								<s:form namespace="/jadwal-kalibrasi" method="pos">
									<s:textfield name="jadwalKalibrasi.namaAlat" placeholder="Nama Alat"
										cssClass="form-control pull-right nama" />
									<div class="input-group-btn">
										<s:submit value="Search" action="SearchJadwalKalibrasi"	cssClass="btn btn-default src-nama" />
									</div>	
									
									<a href="${pageContext.request.contextPath}/jadwal-kalibrasi/AddJadwalKalibrasi.action"
										class="btn btn-success btn-add"><span
										class="glyphicon glyphicon-plus" aria-hidden="true"></span>&nbsp;Tambah</a>
								</s:form>
							</div>
						</div>
					</div>
					<!-- /.box-header -->
					<div class="box-body table-responsive no-padding">
						<s:if test="%{listJadwal.isEmpty()}">
							<table class="table table-hover table-bordered table-empty">
								<thead>
									<tr>
										<td><b>Kode Alat</b></td>
										<td><b>Nama Alat</b></td>
										<td><b>Expired Date</b></td>										
										<td><b>Estimasi Kal</b></td>
										<td><b>Catatan</b></td>
										<td><b>Action</b></td>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td colspan="6">Data yang Anda cari tidak ditemukan,</td>
									</tr>
								</tbody>
							</table>
						</s:if>
						<s:else>
							<display:table id="TablelistJadwal" name="listJadwal"
								pagesize="10" requestURI="/jadwal-kalibrasi/SearchJadwalKalibrasi.action"
								class="table table-hover table-bordered">
								<display:column title="Kode Alat " property="kodeAlat" />
								<display:column title="Nama Alat " property="namaAlat" />
								<display:column title="Expired Date " property="expDate" format="{0,date,dd/MM/yyyy}" />
								<display:column title="Estimasi Kal " property="estimasiKal" format="{0,date,dd/MM/yyyy}" />
								<display:column title="Catatan " property="catatan" />
								<display:column title="Action">
									<a class="btn btn-warning btn-xs"
										href="${pageContext.request.contextPath}/jadwal-kalibrasi/EditJadwalKalibrasi.action?jadwalKalibrasi.idJadwal=<%=((JadwalKalibrasi) pageContext.getAttribute("TablelistJadwal")).getIdJadwal()%>"><span
										class="glyphicon glyphicon-pencil" aria-hidden="true"></span>&nbsp;Edit&nbsp;&nbsp;</a>
									<a class="btn btn-danger btn-xs del"
										href="${pageContext.request.contextPath}/jadwal-kalibrasi/DeleteJadwalKalibrasi.action?jadwalKalibrasi.idJadwal=<%=((JadwalKalibrasi) pageContext.getAttribute("TablelistJadwal")).getIdJadwal()%>"><span
										class="glyphicon glyphicon-trash" aria-hidden="true"></span>&nbsp;Delete</a>
								</display:column>
							</display:table>
						</s:else>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
		</section>
	</div>
	</div>
</body>
</html>