package com.id.kalibrasi.core.action;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.id.kalibrasi.core.mapper.UserMapper;
import com.id.kalibrasi.core.model.User;
import com.id.kalibrasi.core.model.UserExample;

public class UserAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private User user;
	private List<User> listUser = new ArrayList<>();
	private Map<String, Object> session;
	private UserMapper userMapper = (UserMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("userMapper");
	
	public String Login(){
		System.out.println("Jalankan Method Login");
		
		UserExample ex = new UserExample();
		ex.createCriteria().andUserNameEqualTo(user.getUserName()).andPasswordEqualTo(user.getPassword()).andHakAksesEqualTo(user.getHakAkses());
		
		long x=0;
		x = userMapper.countByExample(ex);
		
		if(x>0){
			session.put("userName",user);
			if(user.getHakAkses().equalsIgnoreCase("admin") && session != null){
				user =(User) session.get("userName");
				return "admin";
			}else if(user.getHakAkses().equalsIgnoreCase("qa") && session != null){
				user =(User) session.get("userName");
				return "qa";
			}else if(user.getHakAkses().equalsIgnoreCase("user") && session != null){
				user =(User) session.get("userName");
				return "user";
			}else{
				return ERROR;
			}
		}else{
			addFieldError("invalid", "Username & Password Salah");
		}
		return ERROR;
	}
	
	public String LogOut(){
		System.out.println("Jalankan method LogOut");
		session.remove("userName");
		return SUCCESS;
	}
	
	public String selectAllUser(){
		System.out.println("Jalankan Method selectAllUser");
		
		if(user == null){
			user = new User();
			user.setUserName("");
		}
		
		UserExample examp = new UserExample();
		examp.createCriteria().andUserNameLike("%"+user.getUserName()+"%");
		listUser = userMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String userById(){
		System.out.println("Jalankan Method userById");
		
		user = userMapper.selectByPrimaryKey(user.getIdUser());
		return SUCCESS;
	}
	
	public String changePassword(){
		System.out.println("jalankan Method changePassword");
		
		userMapper.updateByPrimaryKey(user);
		return SUCCESS;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public List<User> getListUser() {
		return listUser;
	}
	public void setListUser(List<User> listUser) {
		this.listUser = listUser;
	}
	public void setSession(Map<String, Object> session){
		this.session = session;
	}

	public UserMapper getUserMapper() {
		return userMapper;
	}

	public void setUserMapper(UserMapper userMapper) {
		this.userMapper = userMapper;
	}
}
