package com.id.kalibrasi.modul.model;

import java.util.Date;

public class Kalibrator {
    private Integer kodeKalibrator;

    private String namaKalibrator;

    private String noSertifikat;

    private String merkKalibrator;

    private Date expDate;

    private String status;

    public Integer getKodeKalibrator() {
        return kodeKalibrator;
    }

    public void setKodeKalibrator(Integer kodeKalibrator) {
        this.kodeKalibrator = kodeKalibrator;
    }

    public String getNamaKalibrator() {
        return namaKalibrator;
    }

    public void setNamaKalibrator(String namaKalibrator) {
        this.namaKalibrator = namaKalibrator == null ? null : namaKalibrator.trim();
    }

    public String getNoSertifikat() {
        return noSertifikat;
    }

    public void setNoSertifikat(String noSertifikat) {
        this.noSertifikat = noSertifikat == null ? null : noSertifikat.trim();
    }

    public String getMerkKalibrator() {
        return merkKalibrator;
    }

    public void setMerkKalibrator(String merkKalibrator) {
        this.merkKalibrator = merkKalibrator == null ? null : merkKalibrator.trim();
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}