package com.id.kalibrasi.modul.model;

import java.util.Date;

public class PermohonanKalibrasi {
    private String noPermohonan;

    private Date tglPermohonan;

    private String noPenerimaan;

    private Date expAwal;

    private Date expAkhir;

    private String statusPermohonan;

    private String tempatPelaksanaan;

    private String namaAlat;

    private String catatan;

    public String getNoPermohonan() {
        return noPermohonan;
    }

    public void setNoPermohonan(String noPermohonan) {
        this.noPermohonan = noPermohonan == null ? null : noPermohonan.trim();
    }

    public Date getTglPermohonan() {
        return tglPermohonan;
    }

    public void setTglPermohonan(Date tglPermohonan) {
        this.tglPermohonan = tglPermohonan;
    }

    public String getNoPenerimaan() {
        return noPenerimaan;
    }

    public void setNoPenerimaan(String noPenerimaan) {
        this.noPenerimaan = noPenerimaan == null ? null : noPenerimaan.trim();
    }

    public Date getExpAwal() {
        return expAwal;
    }

    public void setExpAwal(Date expAwal) {
        this.expAwal = expAwal;
    }

    public Date getExpAkhir() {
        return expAkhir;
    }

    public void setExpAkhir(Date expAkhir) {
        this.expAkhir = expAkhir;
    }

    public String getStatusPermohonan() {
        return statusPermohonan;
    }

    public void setStatusPermohonan(String statusPermohonan) {
        this.statusPermohonan = statusPermohonan == null ? null : statusPermohonan.trim();
    }

    public String getTempatPelaksanaan() {
        return tempatPelaksanaan;
    }

    public void setTempatPelaksanaan(String tempatPelaksanaan) {
        this.tempatPelaksanaan = tempatPelaksanaan == null ? null : tempatPelaksanaan.trim();
    }

    public String getNamaAlat() {
        return namaAlat;
    }

    public void setNamaAlat(String namaAlat) {
        this.namaAlat = namaAlat == null ? null : namaAlat.trim();
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan == null ? null : catatan.trim();
    }
}