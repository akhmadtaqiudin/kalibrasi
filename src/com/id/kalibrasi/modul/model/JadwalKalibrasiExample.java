package com.id.kalibrasi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class JadwalKalibrasiExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public JadwalKalibrasiExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdJadwalIsNull() {
            addCriterion("id_jadwal is null");
            return (Criteria) this;
        }

        public Criteria andIdJadwalIsNotNull() {
            addCriterion("id_jadwal is not null");
            return (Criteria) this;
        }

        public Criteria andIdJadwalEqualTo(Integer value) {
            addCriterion("id_jadwal =", value, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalNotEqualTo(Integer value) {
            addCriterion("id_jadwal <>", value, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalGreaterThan(Integer value) {
            addCriterion("id_jadwal >", value, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_jadwal >=", value, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalLessThan(Integer value) {
            addCriterion("id_jadwal <", value, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalLessThanOrEqualTo(Integer value) {
            addCriterion("id_jadwal <=", value, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalIn(List<Integer> values) {
            addCriterion("id_jadwal in", values, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalNotIn(List<Integer> values) {
            addCriterion("id_jadwal not in", values, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalBetween(Integer value1, Integer value2) {
            addCriterion("id_jadwal between", value1, value2, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andIdJadwalNotBetween(Integer value1, Integer value2) {
            addCriterion("id_jadwal not between", value1, value2, "idJadwal");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIsNull() {
            addCriterion("kode_alat is null");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIsNotNull() {
            addCriterion("kode_alat is not null");
            return (Criteria) this;
        }

        public Criteria andKodeAlatEqualTo(String value) {
            addCriterion("kode_alat =", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotEqualTo(String value) {
            addCriterion("kode_alat <>", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatGreaterThan(String value) {
            addCriterion("kode_alat >", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatGreaterThanOrEqualTo(String value) {
            addCriterion("kode_alat >=", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLessThan(String value) {
            addCriterion("kode_alat <", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLessThanOrEqualTo(String value) {
            addCriterion("kode_alat <=", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLike(String value) {
            addCriterion("kode_alat like", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotLike(String value) {
            addCriterion("kode_alat not like", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIn(List<String> values) {
            addCriterion("kode_alat in", values, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotIn(List<String> values) {
            addCriterion("kode_alat not in", values, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatBetween(String value1, String value2) {
            addCriterion("kode_alat between", value1, value2, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotBetween(String value1, String value2) {
            addCriterion("kode_alat not between", value1, value2, "kodeAlat");
            return (Criteria) this;
        }
        
        public Criteria andNamaAlatIsNull() {
            addCriterion("nama_alat is null");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIsNotNull() {
            addCriterion("nama_alat is not null");
            return (Criteria) this;
        }

        public Criteria andNamaAlatEqualTo(String value) {
            addCriterion("nama_alat =", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotEqualTo(String value) {
            addCriterion("nama_alat <>", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatGreaterThan(String value) {
            addCriterion("nama_alat >", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatGreaterThanOrEqualTo(String value) {
            addCriterion("nama_alat >=", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLessThan(String value) {
            addCriterion("nama_alat <", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLessThanOrEqualTo(String value) {
            addCriterion("nama_alat <=", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLike(String value) {
            addCriterion("nama_alat like", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotLike(String value) {
            addCriterion("nama_alat not like", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIn(List<String> values) {
            addCriterion("nama_alat in", values, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotIn(List<String> values) {
            addCriterion("nama_alat not in", values, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatBetween(String value1, String value2) {
            addCriterion("nama_alat between", value1, value2, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotBetween(String value1, String value2) {
            addCriterion("nama_alat not between", value1, value2, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalIsNull() {
            addCriterion("estimasi_kal is null");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalIsNotNull() {
            addCriterion("estimasi_kal is not null");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalEqualTo(Date value) {
            addCriterionForJDBCDate("estimasi_kal =", value, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalNotEqualTo(Date value) {
            addCriterionForJDBCDate("estimasi_kal <>", value, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalGreaterThan(Date value) {
            addCriterionForJDBCDate("estimasi_kal >", value, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("estimasi_kal >=", value, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalLessThan(Date value) {
            addCriterionForJDBCDate("estimasi_kal <", value, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("estimasi_kal <=", value, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalIn(List<Date> values) {
            addCriterionForJDBCDate("estimasi_kal in", values, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalNotIn(List<Date> values) {
            addCriterionForJDBCDate("estimasi_kal not in", values, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("estimasi_kal between", value1, value2, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andEstimasiKalNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("estimasi_kal not between", value1, value2, "estimasiKal");
            return (Criteria) this;
        }

        public Criteria andCatatanIsNull() {
            addCriterion("catatan is null");
            return (Criteria) this;
        }

        public Criteria andCatatanIsNotNull() {
            addCriterion("catatan is not null");
            return (Criteria) this;
        }

        public Criteria andCatatanEqualTo(String value) {
            addCriterion("catatan =", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotEqualTo(String value) {
            addCriterion("catatan <>", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanGreaterThan(String value) {
            addCriterion("catatan >", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanGreaterThanOrEqualTo(String value) {
            addCriterion("catatan >=", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLessThan(String value) {
            addCriterion("catatan <", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLessThanOrEqualTo(String value) {
            addCriterion("catatan <=", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLike(String value) {
            addCriterion("catatan like", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotLike(String value) {
            addCriterion("catatan not like", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanIn(List<String> values) {
            addCriterion("catatan in", values, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotIn(List<String> values) {
            addCriterion("catatan not in", values, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanBetween(String value1, String value2) {
            addCriterion("catatan between", value1, value2, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotBetween(String value1, String value2) {
            addCriterion("catatan not between", value1, value2, "catatan");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}