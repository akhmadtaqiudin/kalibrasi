package com.id.kalibrasi.modul.model;

import java.util.Date;

public class ReKalibrasi {
    private Integer id;

    private String jenis;

    private Date tanggalKal;

    private String noLaporan;

    private String kodeAlat;
    
    private String namaAlat;

    private Integer idDept;
    
    private String namaDept;

    private Integer kodeKalibrator;

    private String namaKalibrator;

    private Integer rangeResolusi;

    private String penunjukanKal;

    private String rataRata;

    private String penyimpangan;

    private String kelembaban;

    private Integer suhu;

    private String hasil;

    private String catatan;

    private String pelaksanaKal;

    private String kalSelanjutnya;
    
    private String h1;
    
    private String h2;
    
    private String h3;
    
    private String h4;
    
    private String h5;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis == null ? null : jenis.trim();
    }

    public Date getTanggalKal() {
        return tanggalKal;
    }

    public void setTanggalKal(Date tanggalKal) {
        this.tanggalKal = tanggalKal;
    }

    public String getNoLaporan() {
        return noLaporan;
    }

    public void setNoLaporan(String noLaporan) {
        this.noLaporan = noLaporan == null ? null : noLaporan.trim();
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat == null ? null : kodeAlat.trim();
    }

    public Integer getIdDept() {
        return idDept;
    }

    public void setIdDept(Integer idDept) {
        this.idDept = idDept;
    }

    public Integer getKodeKalibrator() {
        return kodeKalibrator;
    }

    public void setKodeKalibrator(Integer kodeKalibrator) {
        this.kodeKalibrator = kodeKalibrator;
    }

    public Integer getRangeResolusi() {
        return rangeResolusi;
    }

    public void setRangeResolusi(Integer rangeResolusi) {
        this.rangeResolusi = rangeResolusi;
    }

    public String getPenunjukanKal() {
        return penunjukanKal;
    }

    public void setPenunjukanKal(String penunjukanKal) {
        this.penunjukanKal = penunjukanKal == null ? null : penunjukanKal.trim();
    }

    public String getRataRata() {
		return rataRata;
	}

	public void setRataRata(String rataRata) {
		this.rataRata = rataRata == null ? null : rataRata.trim();
	}

    public String getPenyimpangan() {
        return penyimpangan;
    }

    public void setPenyimpangan(String penyimpangan) {
        this.penyimpangan = penyimpangan == null ? null : penyimpangan.trim();
    }

    public String getKelembaban() {
        return kelembaban;
    }

    public void setKelembaban(String kelembaban) {
        this.kelembaban = kelembaban == null ? null : kelembaban.trim();
    }

    public Integer getSuhu() {
        return suhu;
    }

    public void setSuhu(Integer suhu) {
        this.suhu = suhu;
    }

    public String getHasil() {
        return hasil;
    }

    public void setHasil(String hasil) {
        this.hasil = hasil == null ? null : hasil.trim();
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan == null ? null : catatan.trim();
    }

    public String getPelaksanaKal() {
        return pelaksanaKal;
    }

    public void setPelaksanaKal(String pelaksanaKal) {
        this.pelaksanaKal = pelaksanaKal == null ? null : pelaksanaKal.trim();
    }

    public String getKalSelanjutnya() {
        return kalSelanjutnya;
    }

    public void setKalSelanjutnya(String kalSelanjutnya) {
        this.kalSelanjutnya = kalSelanjutnya == null ? null : kalSelanjutnya.trim();
    }

	public String getNamaAlat() {
		return namaAlat;
	}

	public void setNamaAlat(String namaAlat) {
		this.namaAlat = namaAlat;
	}

	public String getNamaDept() {
		return namaDept;
	}

	public void setNamaDept(String namaDept) {
		this.namaDept = namaDept;
	}

	public String getNamaKalibrator() {
		return namaKalibrator;
	}

	public void setNamaKalibrator(String namaKalibrator) {
		this.namaKalibrator = namaKalibrator;
	}

	public String getH1() {
		return h1;
	}

	public void setH1(String h1) {
		this.h1 = h1;
	}

	public String getH2() {
		return h2;
	}

	public void setH2(String h2) {
		this.h2 = h2;
	}

	public String getH3() {
		return h3;
	}

	public void setH3(String h3) {
		this.h3 = h3;
	}

	public String getH4() {
		return h4;
	}

	public void setH4(String h4) {
		this.h4 = h4;
	}

	public String getH5() {
		return h5;
	}

	public void setH5(String h5) {
		this.h5 = h5;
	}
}