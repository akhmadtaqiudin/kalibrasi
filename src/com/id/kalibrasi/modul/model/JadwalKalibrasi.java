package com.id.kalibrasi.modul.model;

import java.util.Date;

public class JadwalKalibrasi {
    private Integer idJadwal;

    private String kodeAlat;
    
    private String namaAlat;

    private Date expDate;

    private Date estimasiKal;

    private String catatan;

    public Integer getIdJadwal() {
        return idJadwal;
    }

    public void setIdJadwal(Integer idJadwal) {
        this.idJadwal = idJadwal;
    }

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat == null ? null : kodeAlat.trim();
    }

    public Date getEstimasiKal() {
        return estimasiKal;
    }

    public void setEstimasiKal(Date estimasiKal) {
        this.estimasiKal = estimasiKal;
    }

    public String getCatatan() {
        return catatan;
    }

    public void setCatatan(String catatan) {
        this.catatan = catatan == null ? null : catatan.trim();
    }

	public String getNamaAlat() {
		return namaAlat;
	}

	public void setNamaAlat(String namaAlat) {
		this.namaAlat = namaAlat;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}
}