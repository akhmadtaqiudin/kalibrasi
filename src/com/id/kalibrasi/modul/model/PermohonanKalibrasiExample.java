package com.id.kalibrasi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class PermohonanKalibrasiExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PermohonanKalibrasiExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andNoPermohonanIsNull() {
            addCriterion("no_permohonan is null");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanIsNotNull() {
            addCriterion("no_permohonan is not null");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanEqualTo(String value) {
            addCriterion("no_permohonan =", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanNotEqualTo(String value) {
            addCriterion("no_permohonan <>", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanGreaterThan(String value) {
            addCriterion("no_permohonan >", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanGreaterThanOrEqualTo(String value) {
            addCriterion("no_permohonan >=", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanLessThan(String value) {
            addCriterion("no_permohonan <", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanLessThanOrEqualTo(String value) {
            addCriterion("no_permohonan <=", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanLike(String value) {
            addCriterion("no_permohonan like", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanNotLike(String value) {
            addCriterion("no_permohonan not like", value, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanIn(List<String> values) {
            addCriterion("no_permohonan in", values, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanNotIn(List<String> values) {
            addCriterion("no_permohonan not in", values, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanBetween(String value1, String value2) {
            addCriterion("no_permohonan between", value1, value2, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPermohonanNotBetween(String value1, String value2) {
            addCriterion("no_permohonan not between", value1, value2, "noPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanIsNull() {
            addCriterion("tgl_permohonan is null");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanIsNotNull() {
            addCriterion("tgl_permohonan is not null");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanEqualTo(Date value) {
            addCriterionForJDBCDate("tgl_permohonan =", value, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanNotEqualTo(Date value) {
            addCriterionForJDBCDate("tgl_permohonan <>", value, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanGreaterThan(Date value) {
            addCriterionForJDBCDate("tgl_permohonan >", value, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tgl_permohonan >=", value, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanLessThan(Date value) {
            addCriterionForJDBCDate("tgl_permohonan <", value, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tgl_permohonan <=", value, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanIn(List<Date> values) {
            addCriterionForJDBCDate("tgl_permohonan in", values, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanNotIn(List<Date> values) {
            addCriterionForJDBCDate("tgl_permohonan not in", values, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tgl_permohonan between", value1, value2, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andTglPermohonanNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tgl_permohonan not between", value1, value2, "tglPermohonan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanIsNull() {
            addCriterion("no_penerimaan is null");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanIsNotNull() {
            addCriterion("no_penerimaan is not null");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanEqualTo(String value) {
            addCriterion("no_penerimaan =", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanNotEqualTo(String value) {
            addCriterion("no_penerimaan <>", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanGreaterThan(String value) {
            addCriterion("no_penerimaan >", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanGreaterThanOrEqualTo(String value) {
            addCriterion("no_penerimaan >=", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanLessThan(String value) {
            addCriterion("no_penerimaan <", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanLessThanOrEqualTo(String value) {
            addCriterion("no_penerimaan <=", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanLike(String value) {
            addCriterion("no_penerimaan like", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanNotLike(String value) {
            addCriterion("no_penerimaan not like", value, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanIn(List<String> values) {
            addCriterion("no_penerimaan in", values, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanNotIn(List<String> values) {
            addCriterion("no_penerimaan not in", values, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanBetween(String value1, String value2) {
            addCriterion("no_penerimaan between", value1, value2, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andNoPenerimaanNotBetween(String value1, String value2) {
            addCriterion("no_penerimaan not between", value1, value2, "noPenerimaan");
            return (Criteria) this;
        }

        public Criteria andExpAwalIsNull() {
            addCriterion("exp_awal is null");
            return (Criteria) this;
        }

        public Criteria andExpAwalIsNotNull() {
            addCriterion("exp_awal is not null");
            return (Criteria) this;
        }

        public Criteria andExpAwalEqualTo(Date value) {
            addCriterionForJDBCDate("exp_awal =", value, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalNotEqualTo(Date value) {
            addCriterionForJDBCDate("exp_awal <>", value, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalGreaterThan(Date value) {
            addCriterionForJDBCDate("exp_awal >", value, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_awal >=", value, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalLessThan(Date value) {
            addCriterionForJDBCDate("exp_awal <", value, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_awal <=", value, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalIn(List<Date> values) {
            addCriterionForJDBCDate("exp_awal in", values, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalNotIn(List<Date> values) {
            addCriterionForJDBCDate("exp_awal not in", values, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_awal between", value1, value2, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAwalNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_awal not between", value1, value2, "expAwal");
            return (Criteria) this;
        }

        public Criteria andExpAkhirIsNull() {
            addCriterion("exp_akhir is null");
            return (Criteria) this;
        }

        public Criteria andExpAkhirIsNotNull() {
            addCriterion("exp_akhir is not null");
            return (Criteria) this;
        }

        public Criteria andExpAkhirEqualTo(Date value) {
            addCriterionForJDBCDate("exp_akhir =", value, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirNotEqualTo(Date value) {
            addCriterionForJDBCDate("exp_akhir <>", value, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirGreaterThan(Date value) {
            addCriterionForJDBCDate("exp_akhir >", value, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_akhir >=", value, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirLessThan(Date value) {
            addCriterionForJDBCDate("exp_akhir <", value, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_akhir <=", value, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirIn(List<Date> values) {
            addCriterionForJDBCDate("exp_akhir in", values, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirNotIn(List<Date> values) {
            addCriterionForJDBCDate("exp_akhir not in", values, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_akhir between", value1, value2, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andExpAkhirNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_akhir not between", value1, value2, "expAkhir");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanIsNull() {
            addCriterion("status_permohonan is null");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanIsNotNull() {
            addCriterion("status_permohonan is not null");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanEqualTo(String value) {
            addCriterion("status_permohonan =", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanNotEqualTo(String value) {
            addCriterion("status_permohonan <>", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanGreaterThan(String value) {
            addCriterion("status_permohonan >", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanGreaterThanOrEqualTo(String value) {
            addCriterion("status_permohonan >=", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanLessThan(String value) {
            addCriterion("status_permohonan <", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanLessThanOrEqualTo(String value) {
            addCriterion("status_permohonan <=", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanLike(String value) {
            addCriterion("status_permohonan like", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanNotLike(String value) {
            addCriterion("status_permohonan not like", value, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanIn(List<String> values) {
            addCriterion("status_permohonan in", values, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanNotIn(List<String> values) {
            addCriterion("status_permohonan not in", values, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanBetween(String value1, String value2) {
            addCriterion("status_permohonan between", value1, value2, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andStatusPermohonanNotBetween(String value1, String value2) {
            addCriterion("status_permohonan not between", value1, value2, "statusPermohonan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanIsNull() {
            addCriterion("tempat_pelaksanaan is null");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanIsNotNull() {
            addCriterion("tempat_pelaksanaan is not null");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanEqualTo(String value) {
            addCriterion("tempat_pelaksanaan =", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanNotEqualTo(String value) {
            addCriterion("tempat_pelaksanaan <>", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanGreaterThan(String value) {
            addCriterion("tempat_pelaksanaan >", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanGreaterThanOrEqualTo(String value) {
            addCriterion("tempat_pelaksanaan >=", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanLessThan(String value) {
            addCriterion("tempat_pelaksanaan <", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanLessThanOrEqualTo(String value) {
            addCriterion("tempat_pelaksanaan <=", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanLike(String value) {
            addCriterion("tempat_pelaksanaan like", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanNotLike(String value) {
            addCriterion("tempat_pelaksanaan not like", value, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanIn(List<String> values) {
            addCriterion("tempat_pelaksanaan in", values, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanNotIn(List<String> values) {
            addCriterion("tempat_pelaksanaan not in", values, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanBetween(String value1, String value2) {
            addCriterion("tempat_pelaksanaan between", value1, value2, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andTempatPelaksanaanNotBetween(String value1, String value2) {
            addCriterion("tempat_pelaksanaan not between", value1, value2, "tempatPelaksanaan");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIsNull() {
            addCriterion("nama_alat is null");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIsNotNull() {
            addCriterion("nama_alat is not null");
            return (Criteria) this;
        }

        public Criteria andNamaAlatEqualTo(String value) {
            addCriterion("nama_alat =", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotEqualTo(String value) {
            addCriterion("nama_alat <>", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatGreaterThan(String value) {
            addCriterion("nama_alat >", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatGreaterThanOrEqualTo(String value) {
            addCriterion("nama_alat >=", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLessThan(String value) {
            addCriterion("nama_alat <", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLessThanOrEqualTo(String value) {
            addCriterion("nama_alat <=", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLike(String value) {
            addCriterion("nama_alat like", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotLike(String value) {
            addCriterion("nama_alat not like", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIn(List<String> values) {
            addCriterion("nama_alat in", values, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotIn(List<String> values) {
            addCriterion("nama_alat not in", values, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatBetween(String value1, String value2) {
            addCriterion("nama_alat between", value1, value2, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotBetween(String value1, String value2) {
            addCriterion("nama_alat not between", value1, value2, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andCatatanIsNull() {
            addCriterion("catatan is null");
            return (Criteria) this;
        }

        public Criteria andCatatanIsNotNull() {
            addCriterion("catatan is not null");
            return (Criteria) this;
        }

        public Criteria andCatatanEqualTo(String value) {
            addCriterion("catatan =", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotEqualTo(String value) {
            addCriterion("catatan <>", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanGreaterThan(String value) {
            addCriterion("catatan >", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanGreaterThanOrEqualTo(String value) {
            addCriterion("catatan >=", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLessThan(String value) {
            addCriterion("catatan <", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLessThanOrEqualTo(String value) {
            addCriterion("catatan <=", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLike(String value) {
            addCriterion("catatan like", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotLike(String value) {
            addCriterion("catatan not like", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanIn(List<String> values) {
            addCriterion("catatan in", values, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotIn(List<String> values) {
            addCriterion("catatan not in", values, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanBetween(String value1, String value2) {
            addCriterion("catatan between", value1, value2, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotBetween(String value1, String value2) {
            addCriterion("catatan not between", value1, value2, "catatan");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}