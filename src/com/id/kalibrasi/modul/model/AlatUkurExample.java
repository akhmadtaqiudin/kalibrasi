package com.id.kalibrasi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class AlatUkurExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public AlatUkurExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andKodeAlatIsNull() {
            addCriterion("kode_alat is null");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIsNotNull() {
            addCriterion("kode_alat is not null");
            return (Criteria) this;
        }

        public Criteria andKodeAlatEqualTo(String value) {
            addCriterion("kode_alat =", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotEqualTo(String value) {
            addCriterion("kode_alat <>", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatGreaterThan(String value) {
            addCriterion("kode_alat >", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatGreaterThanOrEqualTo(String value) {
            addCriterion("kode_alat >=", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLessThan(String value) {
            addCriterion("kode_alat <", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLessThanOrEqualTo(String value) {
            addCriterion("kode_alat <=", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIn(List<String> values) {
            addCriterion("kode_alat in", values, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotIn(List<String> values) {
            addCriterion("kode_alat not in", values, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatBetween(String value1, String value2) {
            addCriterion("kode_alat between", value1, value2, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotBetween(String value1, String value2) {
            addCriterion("kode_alat not between", value1, value2, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andDeptIsNull() {
            addCriterion("dept is null");
            return (Criteria) this;
        }

        public Criteria andDeptIsNotNull() {
            addCriterion("dept is not null");
            return (Criteria) this;
        }

        public Criteria andDeptEqualTo(String value) {
            addCriterion("dept =", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotEqualTo(String value) {
            addCriterion("dept <>", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptGreaterThan(String value) {
            addCriterion("dept >", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptGreaterThanOrEqualTo(String value) {
            addCriterion("dept >=", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptLessThan(String value) {
            addCriterion("dept <", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptLessThanOrEqualTo(String value) {
            addCriterion("dept <=", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptLike(String value) {
            addCriterion("dept like", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotLike(String value) {
            addCriterion("dept not like", value, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptIn(List<String> values) {
            addCriterion("dept in", values, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotIn(List<String> values) {
            addCriterion("dept not in", values, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptBetween(String value1, String value2) {
            addCriterion("dept between", value1, value2, "dept");
            return (Criteria) this;
        }

        public Criteria andDeptNotBetween(String value1, String value2) {
            addCriterion("dept not between", value1, value2, "dept");
            return (Criteria) this;
        }

        public Criteria andExpDateIsNull() {
            addCriterion("exp_date is null");
            return (Criteria) this;
        }

        public Criteria andExpDateIsNotNull() {
            addCriterion("exp_date is not null");
            return (Criteria) this;
        }

        public Criteria andExpDateEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date =", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date <>", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateGreaterThan(Date value) {
            addCriterionForJDBCDate("exp_date >", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date >=", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateLessThan(Date value) {
            addCriterionForJDBCDate("exp_date <", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date <=", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateIn(List<Date> values) {
            addCriterionForJDBCDate("exp_date in", values, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("exp_date not in", values, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_date between", value1, value2, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_date not between", value1, value2, "expDate");
            return (Criteria) this;
        }

        public Criteria andIntervalKalIsNull() {
            addCriterion("interval_kal is null");
            return (Criteria) this;
        }

        public Criteria andIntervalKalIsNotNull() {
            addCriterion("interval_kal is not null");
            return (Criteria) this;
        }

        public Criteria andIntervalKalEqualTo(String value) {
            addCriterion("interval_kal =", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalNotEqualTo(String value) {
            addCriterion("interval_kal <>", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalGreaterThan(String value) {
            addCriterion("interval_kal >", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalGreaterThanOrEqualTo(String value) {
            addCriterion("interval_kal >=", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalLessThan(String value) {
            addCriterion("interval_kal <", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalLessThanOrEqualTo(String value) {
            addCriterion("interval_kal <=", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalLike(String value) {
            addCriterion("interval_kal like", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalNotLike(String value) {
            addCriterion("interval_kal not like", value, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalIn(List<String> values) {
            addCriterion("interval_kal in", values, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalNotIn(List<String> values) {
            addCriterion("interval_kal not in", values, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalBetween(String value1, String value2) {
            addCriterion("interval_kal between", value1, value2, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andIntervalKalNotBetween(String value1, String value2) {
            addCriterion("interval_kal not between", value1, value2, "intervalKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalIsNull() {
            addCriterion("jenis_kal is null");
            return (Criteria) this;
        }

        public Criteria andJenisKalIsNotNull() {
            addCriterion("jenis_kal is not null");
            return (Criteria) this;
        }

        public Criteria andJenisKalEqualTo(String value) {
            addCriterion("jenis_kal =", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalNotEqualTo(String value) {
            addCriterion("jenis_kal <>", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalGreaterThan(String value) {
            addCriterion("jenis_kal >", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalGreaterThanOrEqualTo(String value) {
            addCriterion("jenis_kal >=", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalLessThan(String value) {
            addCriterion("jenis_kal <", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalLessThanOrEqualTo(String value) {
            addCriterion("jenis_kal <=", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalLike(String value) {
            addCriterion("jenis_kal like", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalNotLike(String value) {
            addCriterion("jenis_kal not like", value, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalIn(List<String> values) {
            addCriterion("jenis_kal in", values, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalNotIn(List<String> values) {
            addCriterion("jenis_kal not in", values, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalBetween(String value1, String value2) {
            addCriterion("jenis_kal between", value1, value2, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andJenisKalNotBetween(String value1, String value2) {
            addCriterion("jenis_kal not between", value1, value2, "jenisKal");
            return (Criteria) this;
        }

        public Criteria andMerkAlatIsNull() {
            addCriterion("merk_alat is null");
            return (Criteria) this;
        }

        public Criteria andMerkAlatIsNotNull() {
            addCriterion("merk_alat is not null");
            return (Criteria) this;
        }

        public Criteria andMerkAlatEqualTo(String value) {
            addCriterion("merk_alat =", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatNotEqualTo(String value) {
            addCriterion("merk_alat <>", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatGreaterThan(String value) {
            addCriterion("merk_alat >", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatGreaterThanOrEqualTo(String value) {
            addCriterion("merk_alat >=", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatLessThan(String value) {
            addCriterion("merk_alat <", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatLessThanOrEqualTo(String value) {
            addCriterion("merk_alat <=", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatLike(String value) {
            addCriterion("merk_alat like", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatNotLike(String value) {
            addCriterion("merk_alat not like", value, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatIn(List<String> values) {
            addCriterion("merk_alat in", values, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatNotIn(List<String> values) {
            addCriterion("merk_alat not in", values, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatBetween(String value1, String value2) {
            addCriterion("merk_alat between", value1, value2, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andMerkAlatNotBetween(String value1, String value2) {
            addCriterion("merk_alat not between", value1, value2, "merkAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIsNull() {
            addCriterion("nama_alat is null");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIsNotNull() {
            addCriterion("nama_alat is not null");
            return (Criteria) this;
        }

        public Criteria andNamaAlatEqualTo(String value) {
            addCriterion("nama_alat =", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotEqualTo(String value) {
            addCriterion("nama_alat <>", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatGreaterThan(String value) {
            addCriterion("nama_alat >", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatGreaterThanOrEqualTo(String value) {
            addCriterion("nama_alat >=", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLessThan(String value) {
            addCriterion("nama_alat <", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLessThanOrEqualTo(String value) {
            addCriterion("nama_alat <=", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatLike(String value) {
            addCriterion("nama_alat like", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotLike(String value) {
            addCriterion("nama_alat not like", value, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatIn(List<String> values) {
            addCriterion("nama_alat in", values, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotIn(List<String> values) {
            addCriterion("nama_alat not in", values, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatBetween(String value1, String value2) {
            addCriterion("nama_alat between", value1, value2, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNamaAlatNotBetween(String value1, String value2) {
            addCriterion("nama_alat not between", value1, value2, "namaAlat");
            return (Criteria) this;
        }

        public Criteria andNoSeriIsNull() {
            addCriterion("no_seri is null");
            return (Criteria) this;
        }

        public Criteria andNoSeriIsNotNull() {
            addCriterion("no_seri is not null");
            return (Criteria) this;
        }

        public Criteria andNoSeriEqualTo(String value) {
            addCriterion("no_seri =", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriNotEqualTo(String value) {
            addCriterion("no_seri <>", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriGreaterThan(String value) {
            addCriterion("no_seri >", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriGreaterThanOrEqualTo(String value) {
            addCriterion("no_seri >=", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriLessThan(String value) {
            addCriterion("no_seri <", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriLessThanOrEqualTo(String value) {
            addCriterion("no_seri <=", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriLike(String value) {
            addCriterion("no_seri like", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriNotLike(String value) {
            addCriterion("no_seri not like", value, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriIn(List<String> values) {
            addCriterion("no_seri in", values, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriNotIn(List<String> values) {
            addCriterion("no_seri not in", values, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriBetween(String value1, String value2) {
            addCriterion("no_seri between", value1, value2, "noSeri");
            return (Criteria) this;
        }

        public Criteria andNoSeriNotBetween(String value1, String value2) {
            addCriterion("no_seri not between", value1, value2, "noSeri");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}