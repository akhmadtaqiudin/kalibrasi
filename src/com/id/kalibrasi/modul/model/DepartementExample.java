package com.id.kalibrasi.modul.model;

import java.util.ArrayList;
import java.util.List;

public class DepartementExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DepartementExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdDeptIsNull() {
            addCriterion("id_dept is null");
            return (Criteria) this;
        }

        public Criteria andIdDeptIsNotNull() {
            addCriterion("id_dept is not null");
            return (Criteria) this;
        }

        public Criteria andIdDeptEqualTo(Integer value) {
            addCriterion("id_dept =", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptNotEqualTo(Integer value) {
            addCriterion("id_dept <>", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptGreaterThan(Integer value) {
            addCriterion("id_dept >", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_dept >=", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptLessThan(Integer value) {
            addCriterion("id_dept <", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptLessThanOrEqualTo(Integer value) {
            addCriterion("id_dept <=", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptIn(List<Integer> values) {
            addCriterion("id_dept in", values, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptNotIn(List<Integer> values) {
            addCriterion("id_dept not in", values, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptBetween(Integer value1, Integer value2) {
            addCriterion("id_dept between", value1, value2, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptNotBetween(Integer value1, Integer value2) {
            addCriterion("id_dept not between", value1, value2, "idDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptIsNull() {
            addCriterion("nama_dept is null");
            return (Criteria) this;
        }

        public Criteria andNamaDeptIsNotNull() {
            addCriterion("nama_dept is not null");
            return (Criteria) this;
        }

        public Criteria andNamaDeptEqualTo(String value) {
            addCriterion("nama_dept =", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptNotEqualTo(String value) {
            addCriterion("nama_dept <>", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptGreaterThan(String value) {
            addCriterion("nama_dept >", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptGreaterThanOrEqualTo(String value) {
            addCriterion("nama_dept >=", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptLessThan(String value) {
            addCriterion("nama_dept <", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptLessThanOrEqualTo(String value) {
            addCriterion("nama_dept <=", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptLike(String value) {
            addCriterion("nama_dept like", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptNotLike(String value) {
            addCriterion("nama_dept not like", value, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptIn(List<String> values) {
            addCriterion("nama_dept in", values, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptNotIn(List<String> values) {
            addCriterion("nama_dept not in", values, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptBetween(String value1, String value2) {
            addCriterion("nama_dept between", value1, value2, "namaDept");
            return (Criteria) this;
        }

        public Criteria andNamaDeptNotBetween(String value1, String value2) {
            addCriterion("nama_dept not between", value1, value2, "namaDept");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}