package com.id.kalibrasi.modul.model;

public class Departement {
    private Integer idDept;

    private String namaDept;

    public Integer getIdDept() {
        return idDept;
    }

    public void setIdDept(Integer idDept) {
        this.idDept = idDept;
    }

    public String getNamaDept() {
        return namaDept;
    }

    public void setNamaDept(String namaDept) {
        this.namaDept = namaDept == null ? null : namaDept.trim();
    }
}