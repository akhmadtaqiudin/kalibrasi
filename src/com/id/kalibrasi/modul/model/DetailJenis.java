package com.id.kalibrasi.modul.model;

import java.util.Date;

public class DetailJenis {
    private Integer idDetail;

    private String jenis;

    private Date tanggalKal;

    private Integer hasilUkur1;

    private Integer hasilUkur2;

    private Integer hasilUkur3;

    private Integer hasilUkur4;

    private Integer hasilUkur5;

    public Integer getIdDetail() {
        return idDetail;
    }

    public void setIdDetail(Integer idDetail) {
        this.idDetail = idDetail;
    }

    public String getJenis() {
        return jenis;
    }

    public void setJenis(String jenis) {
        this.jenis = jenis == null ? null : jenis.trim();
    }

    public Date getTanggalKal() {
        return tanggalKal;
    }

    public void setTanggalKal(Date tanggalKal) {
        this.tanggalKal = tanggalKal;
    }

    public Integer getHasilUkur1() {
        return hasilUkur1;
    }

    public void setHasilUkur1(Integer hasilUkur1) {
        this.hasilUkur1 = hasilUkur1;
    }

    public Integer getHasilUkur2() {
        return hasilUkur2;
    }

    public void setHasilUkur2(Integer hasilUkur2) {
        this.hasilUkur2 = hasilUkur2;
    }

    public Integer getHasilUkur3() {
        return hasilUkur3;
    }

    public void setHasilUkur3(Integer hasilUkur3) {
        this.hasilUkur3 = hasilUkur3;
    }

    public Integer getHasilUkur4() {
        return hasilUkur4;
    }

    public void setHasilUkur4(Integer hasilUkur4) {
        this.hasilUkur4 = hasilUkur4;
    }

    public Integer getHasilUkur5() {
        return hasilUkur5;
    }

    public void setHasilUkur5(Integer hasilUkur5) {
        this.hasilUkur5 = hasilUkur5;
    }
}