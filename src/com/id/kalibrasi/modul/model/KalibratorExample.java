package com.id.kalibrasi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class KalibratorExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public KalibratorExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andKodeKalibratorIsNull() {
            addCriterion("kode_kalibrator is null");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorIsNotNull() {
            addCriterion("kode_kalibrator is not null");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorEqualTo(Integer value) {
            addCriterion("kode_kalibrator =", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorNotEqualTo(Integer value) {
            addCriterion("kode_kalibrator <>", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorGreaterThan(Integer value) {
            addCriterion("kode_kalibrator >", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorGreaterThanOrEqualTo(Integer value) {
            addCriterion("kode_kalibrator >=", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorLessThan(Integer value) {
            addCriterion("kode_kalibrator <", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorLessThanOrEqualTo(Integer value) {
            addCriterion("kode_kalibrator <=", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorIn(List<Integer> values) {
            addCriterion("kode_kalibrator in", values, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorNotIn(List<Integer> values) {
            addCriterion("kode_kalibrator not in", values, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorBetween(Integer value1, Integer value2) {
            addCriterion("kode_kalibrator between", value1, value2, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorNotBetween(Integer value1, Integer value2) {
            addCriterion("kode_kalibrator not between", value1, value2, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorIsNull() {
            addCriterion("nama_kalibrator is null");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorIsNotNull() {
            addCriterion("nama_kalibrator is not null");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorEqualTo(String value) {
            addCriterion("nama_kalibrator =", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorNotEqualTo(String value) {
            addCriterion("nama_kalibrator <>", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorGreaterThan(String value) {
            addCriterion("nama_kalibrator >", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorGreaterThanOrEqualTo(String value) {
            addCriterion("nama_kalibrator >=", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorLessThan(String value) {
            addCriterion("nama_kalibrator <", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorLessThanOrEqualTo(String value) {
            addCriterion("nama_kalibrator <=", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorLike(String value) {
            addCriterion("nama_kalibrator like", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorNotLike(String value) {
            addCriterion("nama_kalibrator not like", value, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorIn(List<String> values) {
            addCriterion("nama_kalibrator in", values, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorNotIn(List<String> values) {
            addCriterion("nama_kalibrator not in", values, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorBetween(String value1, String value2) {
            addCriterion("nama_kalibrator between", value1, value2, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNamaKalibratorNotBetween(String value1, String value2) {
            addCriterion("nama_kalibrator not between", value1, value2, "namaKalibrator");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatIsNull() {
            addCriterion("no_sertifikat is null");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatIsNotNull() {
            addCriterion("no_sertifikat is not null");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatEqualTo(String value) {
            addCriterion("no_sertifikat =", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatNotEqualTo(String value) {
            addCriterion("no_sertifikat <>", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatGreaterThan(String value) {
            addCriterion("no_sertifikat >", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatGreaterThanOrEqualTo(String value) {
            addCriterion("no_sertifikat >=", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatLessThan(String value) {
            addCriterion("no_sertifikat <", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatLessThanOrEqualTo(String value) {
            addCriterion("no_sertifikat <=", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatLike(String value) {
            addCriterion("no_sertifikat like", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatNotLike(String value) {
            addCriterion("no_sertifikat not like", value, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatIn(List<String> values) {
            addCriterion("no_sertifikat in", values, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatNotIn(List<String> values) {
            addCriterion("no_sertifikat not in", values, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatBetween(String value1, String value2) {
            addCriterion("no_sertifikat between", value1, value2, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andNoSertifikatNotBetween(String value1, String value2) {
            addCriterion("no_sertifikat not between", value1, value2, "noSertifikat");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorIsNull() {
            addCriterion("merk_kalibrator is null");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorIsNotNull() {
            addCriterion("merk_kalibrator is not null");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorEqualTo(String value) {
            addCriterion("merk_kalibrator =", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorNotEqualTo(String value) {
            addCriterion("merk_kalibrator <>", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorGreaterThan(String value) {
            addCriterion("merk_kalibrator >", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorGreaterThanOrEqualTo(String value) {
            addCriterion("merk_kalibrator >=", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorLessThan(String value) {
            addCriterion("merk_kalibrator <", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorLessThanOrEqualTo(String value) {
            addCriterion("merk_kalibrator <=", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorLike(String value) {
            addCriterion("merk_kalibrator like", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorNotLike(String value) {
            addCriterion("merk_kalibrator not like", value, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorIn(List<String> values) {
            addCriterion("merk_kalibrator in", values, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorNotIn(List<String> values) {
            addCriterion("merk_kalibrator not in", values, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorBetween(String value1, String value2) {
            addCriterion("merk_kalibrator between", value1, value2, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andMerkKalibratorNotBetween(String value1, String value2) {
            addCriterion("merk_kalibrator not between", value1, value2, "merkKalibrator");
            return (Criteria) this;
        }

        public Criteria andExpDateIsNull() {
            addCriterion("exp_date is null");
            return (Criteria) this;
        }

        public Criteria andExpDateIsNotNull() {
            addCriterion("exp_date is not null");
            return (Criteria) this;
        }

        public Criteria andExpDateEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date =", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateNotEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date <>", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateGreaterThan(Date value) {
            addCriterionForJDBCDate("exp_date >", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date >=", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateLessThan(Date value) {
            addCriterionForJDBCDate("exp_date <", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("exp_date <=", value, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateIn(List<Date> values) {
            addCriterionForJDBCDate("exp_date in", values, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateNotIn(List<Date> values) {
            addCriterionForJDBCDate("exp_date not in", values, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_date between", value1, value2, "expDate");
            return (Criteria) this;
        }

        public Criteria andExpDateNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("exp_date not between", value1, value2, "expDate");
            return (Criteria) this;
        }

        public Criteria andStatusIsNull() {
            addCriterion("status is null");
            return (Criteria) this;
        }

        public Criteria andStatusIsNotNull() {
            addCriterion("status is not null");
            return (Criteria) this;
        }

        public Criteria andStatusEqualTo(String value) {
            addCriterion("status =", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotEqualTo(String value) {
            addCriterion("status <>", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThan(String value) {
            addCriterion("status >", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusGreaterThanOrEqualTo(String value) {
            addCriterion("status >=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThan(String value) {
            addCriterion("status <", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLessThanOrEqualTo(String value) {
            addCriterion("status <=", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusLike(String value) {
            addCriterion("status like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotLike(String value) {
            addCriterion("status not like", value, "status");
            return (Criteria) this;
        }

        public Criteria andStatusIn(List<String> values) {
            addCriterion("status in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotIn(List<String> values) {
            addCriterion("status not in", values, "status");
            return (Criteria) this;
        }

        public Criteria andStatusBetween(String value1, String value2) {
            addCriterion("status between", value1, value2, "status");
            return (Criteria) this;
        }

        public Criteria andStatusNotBetween(String value1, String value2) {
            addCriterion("status not between", value1, value2, "status");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}