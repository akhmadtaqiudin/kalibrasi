package com.id.kalibrasi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class DetailJenisExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public DetailJenisExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdDetailIsNull() {
            addCriterion("id_detail is null");
            return (Criteria) this;
        }

        public Criteria andIdDetailIsNotNull() {
            addCriterion("id_detail is not null");
            return (Criteria) this;
        }

        public Criteria andIdDetailEqualTo(Integer value) {
            addCriterion("id_detail =", value, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailNotEqualTo(Integer value) {
            addCriterion("id_detail <>", value, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailGreaterThan(Integer value) {
            addCriterion("id_detail >", value, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_detail >=", value, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailLessThan(Integer value) {
            addCriterion("id_detail <", value, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailLessThanOrEqualTo(Integer value) {
            addCriterion("id_detail <=", value, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailIn(List<Integer> values) {
            addCriterion("id_detail in", values, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailNotIn(List<Integer> values) {
            addCriterion("id_detail not in", values, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailBetween(Integer value1, Integer value2) {
            addCriterion("id_detail between", value1, value2, "idDetail");
            return (Criteria) this;
        }

        public Criteria andIdDetailNotBetween(Integer value1, Integer value2) {
            addCriterion("id_detail not between", value1, value2, "idDetail");
            return (Criteria) this;
        }

        public Criteria andJenisIsNull() {
            addCriterion("jenis is null");
            return (Criteria) this;
        }

        public Criteria andJenisIsNotNull() {
            addCriterion("jenis is not null");
            return (Criteria) this;
        }

        public Criteria andJenisEqualTo(String value) {
            addCriterion("jenis =", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotEqualTo(String value) {
            addCriterion("jenis <>", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisGreaterThan(String value) {
            addCriterion("jenis >", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisGreaterThanOrEqualTo(String value) {
            addCriterion("jenis >=", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisLessThan(String value) {
            addCriterion("jenis <", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisLessThanOrEqualTo(String value) {
            addCriterion("jenis <=", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisLike(String value) {
            addCriterion("jenis like", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotLike(String value) {
            addCriterion("jenis not like", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisIn(List<String> values) {
            addCriterion("jenis in", values, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotIn(List<String> values) {
            addCriterion("jenis not in", values, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisBetween(String value1, String value2) {
            addCriterion("jenis between", value1, value2, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotBetween(String value1, String value2) {
            addCriterion("jenis not between", value1, value2, "jenis");
            return (Criteria) this;
        }

        public Criteria andTanggalKalIsNull() {
            addCriterion("tanggal_kal is null");
            return (Criteria) this;
        }

        public Criteria andTanggalKalIsNotNull() {
            addCriterion("tanggal_kal is not null");
            return (Criteria) this;
        }

        public Criteria andTanggalKalEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal =", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalNotEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal <>", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalGreaterThan(Date value) {
            addCriterionForJDBCDate("tanggal_kal >", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal >=", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalLessThan(Date value) {
            addCriterionForJDBCDate("tanggal_kal <", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal <=", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_kal in", values, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalNotIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_kal not in", values, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_kal between", value1, value2, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_kal not between", value1, value2, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1IsNull() {
            addCriterion("hasil_ukur_1 is null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1IsNotNull() {
            addCriterion("hasil_ukur_1 is not null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1EqualTo(Integer value) {
            addCriterion("hasil_ukur_1 =", value, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1NotEqualTo(Integer value) {
            addCriterion("hasil_ukur_1 <>", value, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1GreaterThan(Integer value) {
            addCriterion("hasil_ukur_1 >", value, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1GreaterThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_1 >=", value, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1LessThan(Integer value) {
            addCriterion("hasil_ukur_1 <", value, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1LessThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_1 <=", value, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1In(List<Integer> values) {
            addCriterion("hasil_ukur_1 in", values, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1NotIn(List<Integer> values) {
            addCriterion("hasil_ukur_1 not in", values, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1Between(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_1 between", value1, value2, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur1NotBetween(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_1 not between", value1, value2, "hasilUkur1");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2IsNull() {
            addCriterion("hasil_ukur_2 is null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2IsNotNull() {
            addCriterion("hasil_ukur_2 is not null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2EqualTo(Integer value) {
            addCriterion("hasil_ukur_2 =", value, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2NotEqualTo(Integer value) {
            addCriterion("hasil_ukur_2 <>", value, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2GreaterThan(Integer value) {
            addCriterion("hasil_ukur_2 >", value, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2GreaterThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_2 >=", value, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2LessThan(Integer value) {
            addCriterion("hasil_ukur_2 <", value, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2LessThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_2 <=", value, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2In(List<Integer> values) {
            addCriterion("hasil_ukur_2 in", values, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2NotIn(List<Integer> values) {
            addCriterion("hasil_ukur_2 not in", values, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2Between(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_2 between", value1, value2, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur2NotBetween(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_2 not between", value1, value2, "hasilUkur2");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3IsNull() {
            addCriterion("hasil_ukur_3 is null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3IsNotNull() {
            addCriterion("hasil_ukur_3 is not null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3EqualTo(Integer value) {
            addCriterion("hasil_ukur_3 =", value, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3NotEqualTo(Integer value) {
            addCriterion("hasil_ukur_3 <>", value, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3GreaterThan(Integer value) {
            addCriterion("hasil_ukur_3 >", value, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3GreaterThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_3 >=", value, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3LessThan(Integer value) {
            addCriterion("hasil_ukur_3 <", value, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3LessThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_3 <=", value, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3In(List<Integer> values) {
            addCriterion("hasil_ukur_3 in", values, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3NotIn(List<Integer> values) {
            addCriterion("hasil_ukur_3 not in", values, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3Between(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_3 between", value1, value2, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur3NotBetween(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_3 not between", value1, value2, "hasilUkur3");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4IsNull() {
            addCriterion("hasil_ukur_4 is null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4IsNotNull() {
            addCriterion("hasil_ukur_4 is not null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4EqualTo(Integer value) {
            addCriterion("hasil_ukur_4 =", value, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4NotEqualTo(Integer value) {
            addCriterion("hasil_ukur_4 <>", value, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4GreaterThan(Integer value) {
            addCriterion("hasil_ukur_4 >", value, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4GreaterThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_4 >=", value, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4LessThan(Integer value) {
            addCriterion("hasil_ukur_4 <", value, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4LessThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_4 <=", value, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4In(List<Integer> values) {
            addCriterion("hasil_ukur_4 in", values, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4NotIn(List<Integer> values) {
            addCriterion("hasil_ukur_4 not in", values, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4Between(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_4 between", value1, value2, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur4NotBetween(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_4 not between", value1, value2, "hasilUkur4");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5IsNull() {
            addCriterion("hasil_ukur_5 is null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5IsNotNull() {
            addCriterion("hasil_ukur_5 is not null");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5EqualTo(Integer value) {
            addCriterion("hasil_ukur_5 =", value, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5NotEqualTo(Integer value) {
            addCriterion("hasil_ukur_5 <>", value, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5GreaterThan(Integer value) {
            addCriterion("hasil_ukur_5 >", value, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5GreaterThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_5 >=", value, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5LessThan(Integer value) {
            addCriterion("hasil_ukur_5 <", value, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5LessThanOrEqualTo(Integer value) {
            addCriterion("hasil_ukur_5 <=", value, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5In(List<Integer> values) {
            addCriterion("hasil_ukur_5 in", values, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5NotIn(List<Integer> values) {
            addCriterion("hasil_ukur_5 not in", values, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5Between(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_5 between", value1, value2, "hasilUkur5");
            return (Criteria) this;
        }

        public Criteria andHasilUkur5NotBetween(Integer value1, Integer value2) {
            addCriterion("hasil_ukur_5 not between", value1, value2, "hasilUkur5");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}