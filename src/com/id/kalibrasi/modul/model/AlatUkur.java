package com.id.kalibrasi.modul.model;

import java.util.Date;

public class AlatUkur {
    private String kodeAlat;

    private String dept;

    private Date expDate;

    private String intervalKal;

    private String jenisKal;

    private String merkAlat;

    private String namaAlat;

    private String noSeri;

    private String status;

    public String getKodeAlat() {
        return kodeAlat;
    }

    public void setKodeAlat(String kodeAlat) {
        this.kodeAlat = kodeAlat;
    }

    public String getDept() {
        return dept;
    }

    public void setDept(String dept) {
        this.dept = dept == null ? null : dept.trim();
    }

    public Date getExpDate() {
        return expDate;
    }

    public void setExpDate(Date expDate) {
        this.expDate = expDate;
    }

    public String getIntervalKal() {
        return intervalKal;
    }

    public void setIntervalKal(String intervalKal) {
        this.intervalKal = intervalKal == null ? null : intervalKal.trim();
    }

    public String getJenisKal() {
        return jenisKal;
    }

    public void setJenisKal(String jenisKal) {
        this.jenisKal = jenisKal == null ? null : jenisKal.trim();
    }

    public String getMerkAlat() {
        return merkAlat;
    }

    public void setMerkAlat(String merkAlat) {
        this.merkAlat = merkAlat == null ? null : merkAlat.trim();
    }

    public String getNamaAlat() {
        return namaAlat;
    }

    public void setNamaAlat(String namaAlat) {
        this.namaAlat = namaAlat == null ? null : namaAlat.trim();
    }

    public String getNoSeri() {
        return noSeri;
    }

    public void setNoSeri(String noSeri) {
        this.noSeri = noSeri == null ? null : noSeri.trim();
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status == null ? null : status.trim();
    }
}