package com.id.kalibrasi.modul.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class ReKalibrasiExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public ReKalibrasiExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        protected void addCriterionForJDBCDate(String condition, Date value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value.getTime()), property);
        }

        protected void addCriterionForJDBCDate(String condition, List<Date> values, String property) {
            if (values == null || values.size() == 0) {
                throw new RuntimeException("Value list for " + property + " cannot be null or empty");
            }
            List<java.sql.Date> dateList = new ArrayList<java.sql.Date>();
            Iterator<Date> iter = values.iterator();
            while (iter.hasNext()) {
                dateList.add(new java.sql.Date(iter.next().getTime()));
            }
            addCriterion(condition, dateList, property);
        }

        protected void addCriterionForJDBCDate(String condition, Date value1, Date value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            addCriterion(condition, new java.sql.Date(value1.getTime()), new java.sql.Date(value2.getTime()), property);
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Integer value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Integer value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Integer value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Integer value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Integer value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Integer> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Integer> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Integer value1, Integer value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Integer value1, Integer value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andJenisIsNull() {
            addCriterion("jenis is null");
            return (Criteria) this;
        }

        public Criteria andJenisIsNotNull() {
            addCriterion("jenis is not null");
            return (Criteria) this;
        }

        public Criteria andJenisEqualTo(String value) {
            addCriterion("jenis =", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotEqualTo(String value) {
            addCriterion("jenis <>", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisGreaterThan(String value) {
            addCriterion("jenis >", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisGreaterThanOrEqualTo(String value) {
            addCriterion("jenis >=", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisLessThan(String value) {
            addCriterion("jenis <", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisLessThanOrEqualTo(String value) {
            addCriterion("jenis <=", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisLike(String value) {
            addCriterion("jenis like", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotLike(String value) {
            addCriterion("jenis not like", value, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisIn(List<String> values) {
            addCriterion("jenis in", values, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotIn(List<String> values) {
            addCriterion("jenis not in", values, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisBetween(String value1, String value2) {
            addCriterion("jenis between", value1, value2, "jenis");
            return (Criteria) this;
        }

        public Criteria andJenisNotBetween(String value1, String value2) {
            addCriterion("jenis not between", value1, value2, "jenis");
            return (Criteria) this;
        }

        public Criteria andTanggalKalIsNull() {
            addCriterion("tanggal_kal is null");
            return (Criteria) this;
        }

        public Criteria andTanggalKalIsNotNull() {
            addCriterion("tanggal_kal is not null");
            return (Criteria) this;
        }

        public Criteria andTanggalKalEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal =", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalNotEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal <>", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalGreaterThan(Date value) {
            addCriterionForJDBCDate("tanggal_kal >", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalGreaterThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal >=", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalLessThan(Date value) {
            addCriterionForJDBCDate("tanggal_kal <", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalLessThanOrEqualTo(Date value) {
            addCriterionForJDBCDate("tanggal_kal <=", value, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_kal in", values, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalNotIn(List<Date> values) {
            addCriterionForJDBCDate("tanggal_kal not in", values, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_kal between", value1, value2, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andTanggalKalNotBetween(Date value1, Date value2) {
            addCriterionForJDBCDate("tanggal_kal not between", value1, value2, "tanggalKal");
            return (Criteria) this;
        }

        public Criteria andNoLaporanIsNull() {
            addCriterion("no_laporan is null");
            return (Criteria) this;
        }

        public Criteria andNoLaporanIsNotNull() {
            addCriterion("no_laporan is not null");
            return (Criteria) this;
        }

        public Criteria andNoLaporanEqualTo(String value) {
            addCriterion("no_laporan =", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanNotEqualTo(String value) {
            addCriterion("no_laporan <>", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanGreaterThan(String value) {
            addCriterion("no_laporan >", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanGreaterThanOrEqualTo(String value) {
            addCriterion("no_laporan >=", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanLessThan(String value) {
            addCriterion("no_laporan <", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanLessThanOrEqualTo(String value) {
            addCriterion("no_laporan <=", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanLike(String value) {
            addCriterion("no_laporan like", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanNotLike(String value) {
            addCriterion("no_laporan not like", value, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanIn(List<String> values) {
            addCriterion("no_laporan in", values, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanNotIn(List<String> values) {
            addCriterion("no_laporan not in", values, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanBetween(String value1, String value2) {
            addCriterion("no_laporan between", value1, value2, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andNoLaporanNotBetween(String value1, String value2) {
            addCriterion("no_laporan not between", value1, value2, "noLaporan");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIsNull() {
            addCriterion("kode_alat is null");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIsNotNull() {
            addCriterion("kode_alat is not null");
            return (Criteria) this;
        }

        public Criteria andKodeAlatEqualTo(String value) {
            addCriterion("kode_alat =", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotEqualTo(String value) {
            addCriterion("kode_alat <>", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatGreaterThan(String value) {
            addCriterion("kode_alat >", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatGreaterThanOrEqualTo(String value) {
            addCriterion("kode_alat >=", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLessThan(String value) {
            addCriterion("kode_alat <", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLessThanOrEqualTo(String value) {
            addCriterion("kode_alat <=", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatLike(String value) {
            addCriterion("kode_alat like", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotLike(String value) {
            addCriterion("kode_alat not like", value, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatIn(List<String> values) {
            addCriterion("kode_alat in", values, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotIn(List<String> values) {
            addCriterion("kode_alat not in", values, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatBetween(String value1, String value2) {
            addCriterion("kode_alat between", value1, value2, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andKodeAlatNotBetween(String value1, String value2) {
            addCriterion("kode_alat not between", value1, value2, "kodeAlat");
            return (Criteria) this;
        }

        public Criteria andIdDeptIsNull() {
            addCriterion("id_dept is null");
            return (Criteria) this;
        }

        public Criteria andIdDeptIsNotNull() {
            addCriterion("id_dept is not null");
            return (Criteria) this;
        }

        public Criteria andIdDeptEqualTo(Integer value) {
            addCriterion("id_dept =", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptNotEqualTo(Integer value) {
            addCriterion("id_dept <>", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptGreaterThan(Integer value) {
            addCriterion("id_dept >", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptGreaterThanOrEqualTo(Integer value) {
            addCriterion("id_dept >=", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptLessThan(Integer value) {
            addCriterion("id_dept <", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptLessThanOrEqualTo(Integer value) {
            addCriterion("id_dept <=", value, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptIn(List<Integer> values) {
            addCriterion("id_dept in", values, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptNotIn(List<Integer> values) {
            addCriterion("id_dept not in", values, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptBetween(Integer value1, Integer value2) {
            addCriterion("id_dept between", value1, value2, "idDept");
            return (Criteria) this;
        }

        public Criteria andIdDeptNotBetween(Integer value1, Integer value2) {
            addCriterion("id_dept not between", value1, value2, "idDept");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorIsNull() {
            addCriterion("kode_kalibrator is null");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorIsNotNull() {
            addCriterion("kode_kalibrator is not null");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorEqualTo(Integer value) {
            addCriterion("kode_kalibrator =", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorNotEqualTo(Integer value) {
            addCriterion("kode_kalibrator <>", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorGreaterThan(Integer value) {
            addCriterion("kode_kalibrator >", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorGreaterThanOrEqualTo(Integer value) {
            addCriterion("kode_kalibrator >=", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorLessThan(Integer value) {
            addCriterion("kode_kalibrator <", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorLessThanOrEqualTo(Integer value) {
            addCriterion("kode_kalibrator <=", value, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorIn(List<Integer> values) {
            addCriterion("kode_kalibrator in", values, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorNotIn(List<Integer> values) {
            addCriterion("kode_kalibrator not in", values, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorBetween(Integer value1, Integer value2) {
            addCriterion("kode_kalibrator between", value1, value2, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andKodeKalibratorNotBetween(Integer value1, Integer value2) {
            addCriterion("kode_kalibrator not between", value1, value2, "kodeKalibrator");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiIsNull() {
            addCriterion("range_resolusi is null");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiIsNotNull() {
            addCriterion("range_resolusi is not null");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiEqualTo(Integer value) {
            addCriterion("range_resolusi =", value, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiNotEqualTo(Integer value) {
            addCriterion("range_resolusi <>", value, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiGreaterThan(Integer value) {
            addCriterion("range_resolusi >", value, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiGreaterThanOrEqualTo(Integer value) {
            addCriterion("range_resolusi >=", value, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiLessThan(Integer value) {
            addCriterion("range_resolusi <", value, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiLessThanOrEqualTo(Integer value) {
            addCriterion("range_resolusi <=", value, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiIn(List<Integer> values) {
            addCriterion("range_resolusi in", values, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiNotIn(List<Integer> values) {
            addCriterion("range_resolusi not in", values, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiBetween(Integer value1, Integer value2) {
            addCriterion("range_resolusi between", value1, value2, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andRangeResolusiNotBetween(Integer value1, Integer value2) {
            addCriterion("range_resolusi not between", value1, value2, "rangeResolusi");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalIsNull() {
            addCriterion("penunjukan_kal is null");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalIsNotNull() {
            addCriterion("penunjukan_kal is not null");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalEqualTo(String value) {
            addCriterion("penunjukan_kal =", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalNotEqualTo(String value) {
            addCriterion("penunjukan_kal <>", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalGreaterThan(String value) {
            addCriterion("penunjukan_kal >", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalGreaterThanOrEqualTo(String value) {
            addCriterion("penunjukan_kal >=", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalLessThan(String value) {
            addCriterion("penunjukan_kal <", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalLessThanOrEqualTo(String value) {
            addCriterion("penunjukan_kal <=", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalLike(String value) {
            addCriterion("penunjukan_kal like", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalNotLike(String value) {
            addCriterion("penunjukan_kal not like", value, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalIn(List<String> values) {
            addCriterion("penunjukan_kal in", values, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalNotIn(List<String> values) {
            addCriterion("penunjukan_kal not in", values, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalBetween(String value1, String value2) {
            addCriterion("penunjukan_kal between", value1, value2, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andPenunjukanKalNotBetween(String value1, String value2) {
            addCriterion("penunjukan_kal not between", value1, value2, "penunjukanKal");
            return (Criteria) this;
        }

        public Criteria andRataRataIsNull() {
            addCriterion("x_rata is null");
            return (Criteria) this;
        }

        public Criteria andRataRataIsNotNull() {
            addCriterion("x_rata is not null");
            return (Criteria) this;
        }

        public Criteria andRataRataEqualTo(String value) {
            addCriterion("x_rata =", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataNotEqualTo(String value) {
            addCriterion("x_rata <>", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataGreaterThan(String value) {
            addCriterion("x_rata >", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataGreaterThanOrEqualTo(String value) {
            addCriterion("x_rata >=", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataLessThan(String value) {
            addCriterion("x_rata <", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataLessThanOrEqualTo(String value) {
            addCriterion("x_rata <=", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataLike(String value) {
            addCriterion("x_rata like", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataNotLike(String value) {
            addCriterion("x_rata not like", value, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataIn(List<String> values) {
            addCriterion("x_rata in", values, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataNotIn(List<String> values) {
            addCriterion("x_rata not in", values, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataBetween(String value1, String value2) {
            addCriterion("x_rata between", value1, value2, "rataRata");
            return (Criteria) this;
        }

        public Criteria andRataRataNotBetween(String value1, String value2) {
            addCriterion("x_rata not between", value1, value2, "rataRata");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganIsNull() {
            addCriterion("penyimpangan is null");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganIsNotNull() {
            addCriterion("penyimpangan is not null");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganEqualTo(String value) {
            addCriterion("penyimpangan =", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganNotEqualTo(String value) {
            addCriterion("penyimpangan <>", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganGreaterThan(String value) {
            addCriterion("penyimpangan >", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganGreaterThanOrEqualTo(String value) {
            addCriterion("penyimpangan >=", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganLessThan(String value) {
            addCriterion("penyimpangan <", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganLessThanOrEqualTo(String value) {
            addCriterion("penyimpangan <=", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganLike(String value) {
            addCriterion("penyimpangan like", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganNotLike(String value) {
            addCriterion("penyimpangan not like", value, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganIn(List<String> values) {
            addCriterion("penyimpangan in", values, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganNotIn(List<String> values) {
            addCriterion("penyimpangan not in", values, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganBetween(String value1, String value2) {
            addCriterion("penyimpangan between", value1, value2, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andPenyimpanganNotBetween(String value1, String value2) {
            addCriterion("penyimpangan not between", value1, value2, "penyimpangan");
            return (Criteria) this;
        }

        public Criteria andKelembabanIsNull() {
            addCriterion("kelembaban is null");
            return (Criteria) this;
        }

        public Criteria andKelembabanIsNotNull() {
            addCriterion("kelembaban is not null");
            return (Criteria) this;
        }

        public Criteria andKelembabanEqualTo(String value) {
            addCriterion("kelembaban =", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanNotEqualTo(String value) {
            addCriterion("kelembaban <>", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanGreaterThan(String value) {
            addCriterion("kelembaban >", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanGreaterThanOrEqualTo(String value) {
            addCriterion("kelembaban >=", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanLessThan(String value) {
            addCriterion("kelembaban <", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanLessThanOrEqualTo(String value) {
            addCriterion("kelembaban <=", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanLike(String value) {
            addCriterion("kelembaban like", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanNotLike(String value) {
            addCriterion("kelembaban not like", value, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanIn(List<String> values) {
            addCriterion("kelembaban in", values, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanNotIn(List<String> values) {
            addCriterion("kelembaban not in", values, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanBetween(String value1, String value2) {
            addCriterion("kelembaban between", value1, value2, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andKelembabanNotBetween(String value1, String value2) {
            addCriterion("kelembaban not between", value1, value2, "kelembaban");
            return (Criteria) this;
        }

        public Criteria andSuhuIsNull() {
            addCriterion("suhu is null");
            return (Criteria) this;
        }

        public Criteria andSuhuIsNotNull() {
            addCriterion("suhu is not null");
            return (Criteria) this;
        }

        public Criteria andSuhuEqualTo(Integer value) {
            addCriterion("suhu =", value, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuNotEqualTo(Integer value) {
            addCriterion("suhu <>", value, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuGreaterThan(Integer value) {
            addCriterion("suhu >", value, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuGreaterThanOrEqualTo(Integer value) {
            addCriterion("suhu >=", value, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuLessThan(Integer value) {
            addCriterion("suhu <", value, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuLessThanOrEqualTo(Integer value) {
            addCriterion("suhu <=", value, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuIn(List<Integer> values) {
            addCriterion("suhu in", values, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuNotIn(List<Integer> values) {
            addCriterion("suhu not in", values, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuBetween(Integer value1, Integer value2) {
            addCriterion("suhu between", value1, value2, "suhu");
            return (Criteria) this;
        }

        public Criteria andSuhuNotBetween(Integer value1, Integer value2) {
            addCriterion("suhu not between", value1, value2, "suhu");
            return (Criteria) this;
        }

        public Criteria andHasilIsNull() {
            addCriterion("hasil is null");
            return (Criteria) this;
        }

        public Criteria andHasilIsNotNull() {
            addCriterion("hasil is not null");
            return (Criteria) this;
        }

        public Criteria andHasilEqualTo(String value) {
            addCriterion("hasil =", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilNotEqualTo(String value) {
            addCriterion("hasil <>", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilGreaterThan(String value) {
            addCriterion("hasil >", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilGreaterThanOrEqualTo(String value) {
            addCriterion("hasil >=", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilLessThan(String value) {
            addCriterion("hasil <", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilLessThanOrEqualTo(String value) {
            addCriterion("hasil <=", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilLike(String value) {
            addCriterion("hasil like", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilNotLike(String value) {
            addCriterion("hasil not like", value, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilIn(List<String> values) {
            addCriterion("hasil in", values, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilNotIn(List<String> values) {
            addCriterion("hasil not in", values, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilBetween(String value1, String value2) {
            addCriterion("hasil between", value1, value2, "hasil");
            return (Criteria) this;
        }

        public Criteria andHasilNotBetween(String value1, String value2) {
            addCriterion("hasil not between", value1, value2, "hasil");
            return (Criteria) this;
        }

        public Criteria andCatatanIsNull() {
            addCriterion("catatan is null");
            return (Criteria) this;
        }

        public Criteria andCatatanIsNotNull() {
            addCriterion("catatan is not null");
            return (Criteria) this;
        }

        public Criteria andCatatanEqualTo(String value) {
            addCriterion("catatan =", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotEqualTo(String value) {
            addCriterion("catatan <>", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanGreaterThan(String value) {
            addCriterion("catatan >", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanGreaterThanOrEqualTo(String value) {
            addCriterion("catatan >=", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLessThan(String value) {
            addCriterion("catatan <", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLessThanOrEqualTo(String value) {
            addCriterion("catatan <=", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanLike(String value) {
            addCriterion("catatan like", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotLike(String value) {
            addCriterion("catatan not like", value, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanIn(List<String> values) {
            addCriterion("catatan in", values, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotIn(List<String> values) {
            addCriterion("catatan not in", values, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanBetween(String value1, String value2) {
            addCriterion("catatan between", value1, value2, "catatan");
            return (Criteria) this;
        }

        public Criteria andCatatanNotBetween(String value1, String value2) {
            addCriterion("catatan not between", value1, value2, "catatan");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalIsNull() {
            addCriterion("pelaksana_kal is null");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalIsNotNull() {
            addCriterion("pelaksana_kal is not null");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalEqualTo(String value) {
            addCriterion("pelaksana_kal =", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalNotEqualTo(String value) {
            addCriterion("pelaksana_kal <>", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalGreaterThan(String value) {
            addCriterion("pelaksana_kal >", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalGreaterThanOrEqualTo(String value) {
            addCriterion("pelaksana_kal >=", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalLessThan(String value) {
            addCriterion("pelaksana_kal <", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalLessThanOrEqualTo(String value) {
            addCriterion("pelaksana_kal <=", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalLike(String value) {
            addCriterion("pelaksana_kal like", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalNotLike(String value) {
            addCriterion("pelaksana_kal not like", value, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalIn(List<String> values) {
            addCriterion("pelaksana_kal in", values, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalNotIn(List<String> values) {
            addCriterion("pelaksana_kal not in", values, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalBetween(String value1, String value2) {
            addCriterion("pelaksana_kal between", value1, value2, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andPelaksanaKalNotBetween(String value1, String value2) {
            addCriterion("pelaksana_kal not between", value1, value2, "pelaksanaKal");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaIsNull() {
            addCriterion("kal_selanjutnya is null");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaIsNotNull() {
            addCriterion("kal_selanjutnya is not null");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaEqualTo(String value) {
            addCriterion("kal_selanjutnya =", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaNotEqualTo(String value) {
            addCriterion("kal_selanjutnya <>", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaGreaterThan(String value) {
            addCriterion("kal_selanjutnya >", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaGreaterThanOrEqualTo(String value) {
            addCriterion("kal_selanjutnya >=", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaLessThan(String value) {
            addCriterion("kal_selanjutnya <", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaLessThanOrEqualTo(String value) {
            addCriterion("kal_selanjutnya <=", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaLike(String value) {
            addCriterion("kal_selanjutnya like", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaNotLike(String value) {
            addCriterion("kal_selanjutnya not like", value, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaIn(List<String> values) {
            addCriterion("kal_selanjutnya in", values, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaNotIn(List<String> values) {
            addCriterion("kal_selanjutnya not in", values, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaBetween(String value1, String value2) {
            addCriterion("kal_selanjutnya between", value1, value2, "kalSelanjutnya");
            return (Criteria) this;
        }

        public Criteria andKalSelanjutnyaNotBetween(String value1, String value2) {
            addCriterion("kal_selanjutnya not between", value1, value2, "kalSelanjutnya");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}