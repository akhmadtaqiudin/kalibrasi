package com.id.kalibrasi.modul.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.id.kalibrasi.core.action.CoreAction;
import com.id.kalibrasi.modul.mapper.AlatUkurMapper;
import com.id.kalibrasi.modul.model.AlatUkur;
import com.id.kalibrasi.modul.model.AlatUkurExample;

public class AlatUkurAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private String strDate;
	private AlatUkur alatUkur;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private List<AlatUkur> listAlat = new ArrayList<>();
	private AlatUkurMapper alatUkurMapper = (AlatUkurMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("alatUkurMapper");
	
	public String searchAllAlatUkur(){
		System.out.println("Jalankan method SearchAllAlatUkur");
		
		if(alatUkur == null){
			alatUkur = new AlatUkur();
			alatUkur.setNamaAlat("");
		}
		
		AlatUkurExample examp = new AlatUkurExample();
		examp.createCriteria().andNamaAlatLike("%"+alatUkur.getNamaAlat()+"%");
		listAlat = alatUkurMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("Jalankan method SearchById");		
		
		alatUkur = alatUkurMapper.selectByPrimaryKey(alatUkur.getKodeAlat());
		setStrDate(sdf.format(alatUkur.getExpDate()));
		return SUCCESS;
	}
	
	public String searchList(){
		System.out.println("Jalankan method SearchById");
		
		AlatUkurExample examp = new AlatUkurExample();
		listAlat = alatUkurMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchByName(){
		System.out.println("Jalankan method searchByName");
		
		try {
			//nilai ext = Wed Jan 24 00:00:00 ICT 2018
			SimpleDateFormat sd = new SimpleDateFormat("dd/mm/yyyy");
			alatUkur = alatUkurMapper.selectByName(alatUkur);
			String s;
			s = sd.format(alatUkur.getExpDate());
			alatUkur.setExpDate(sd.parse(s));
			System.out.println("nilai ext = "+alatUkur.getExpDate());
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String saveAlatUkur(){
		System.out.println("Jalankan method SaveAlatUkur");
		
		AlatUkurExample examp = new AlatUkurExample();
		examp.createCriteria().andNamaAlatEqualTo(alatUkur.getNamaAlat()).andMerkAlatEqualTo(alatUkur.getMerkAlat());
		String kode = "K";
		String id = "";
		long con = 0;
		
		con = alatUkurMapper.countByExample(examp);
		id = alatUkurMapper.selectMaxId(alatUkur);
		
		if(con > 0){
			addFieldError("fieldAlat", "Data Nama Alat = "+alatUkur.getNamaAlat()+" dan Merk = "+alatUkur.getMerkAlat()+" sudah tersimpan pada database, harap gunakan data yang lain");
			return ERROR;
		}
		if(id==null){
			id = "0";
		}else{
			id = id.substring(4);
		}
		
		int x = Integer.parseInt(id);
		x++;
		if(x <99 && x>=10){
			kode+="00"+x;
		}else if(x>99 && x<1000){
			kode+="0"+x;
		}else if(x>=999){
			kode+=x;
		}else{
			kode+="000"+x;
		}
		
		try {
			Date ex = sdf.parse(getStrDate());
			
			alatUkur.setKodeAlat(kode);
			alatUkur.setExpDate(ex);
			alatUkurMapper.insert(alatUkur);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String editAlatUkur(){
		System.out.println("Jalankan method editAlatUkur");
		
		try {
			Date ex = sdf.parse(getStrDate());
			alatUkur.setExpDate(ex);
			alatUkurMapper.updateByPrimaryKey(alatUkur);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String deleteAlatUkur(){
		System.out.println("Jalankan method deleteAlatUkur");
		
		alatUkurMapper.deleteByPrimaryKey(alatUkur.getKodeAlat());
		return SUCCESS;
	}
	
	public String getStrDate() {
		return strDate;
	}
	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}
	public AlatUkur getAlatUkur() {
		return alatUkur;
	}
	public void setAlatUkur(AlatUkur alatUkur) {
		this.alatUkur = alatUkur;
	}
	public List<AlatUkur> getListAlat() {
		return listAlat;
	}
	public void setListAlat(List<AlatUkur> listAlat) {
		this.listAlat = listAlat;
	}
	public AlatUkurMapper getAlatUkurMapper() {
		return alatUkurMapper;
	}
	public void setAlatUkurMapper(AlatUkurMapper alatUkurMapper) {
		this.alatUkurMapper = alatUkurMapper;
	}
}
