package com.id.kalibrasi.modul.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.id.kalibrasi.core.action.CoreAction;
import com.id.kalibrasi.modul.mapper.DetailJenisMapper;
import com.id.kalibrasi.modul.mapper.ReKalibrasiMapper;
import com.id.kalibrasi.modul.model.DetailJenis;
import com.id.kalibrasi.modul.model.DetailJenisExample;
import com.id.kalibrasi.modul.model.ReKalibrasi;
import com.id.kalibrasi.modul.model.ReKalibrasiExample;

public class ReKalibrasiAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private ReKalibrasi reKalibrasi;
	private String strDate;
	private Integer[] hasilUkur1;
    private Integer[] hasilUkur2;
    private Integer[] hasilUkur3;
    private Integer[] hasilUkur4;
    private Integer[] hasilUkur5;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	private InputStream inputStreamFile;
	private String reportFile;
	private long contentLength;
	private String fileName;
	
	private List<ReKalibrasi> listReKalibrasi = new ArrayList<>();
	private List<DetailJenis> listDetailJenis = new ArrayList<>();
	private ReKalibrasiMapper reKalibrasiMapper = (ReKalibrasiMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("reKalibrasiMapper");
	private DetailJenisMapper detailJenisMapper = (DetailJenisMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("detailJenisMapper");
	
	public String searchReKalibrasi(){
		System.out.println("Jalankan method searchReKalibrasi");
		
		if(reKalibrasi == null){
			reKalibrasi = new ReKalibrasi();
			reKalibrasi.setJenis("");
		}
		
		ReKalibrasiExample examp = new ReKalibrasiExample();
		examp.createCriteria().andJenisLike("%"+reKalibrasi.getJenis()+"%");
		listReKalibrasi = reKalibrasiMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("Jalankan method searchById");
		
		reKalibrasi = reKalibrasiMapper.selectByPrimaryKey(reKalibrasi.getId());
		setStrDate(sdf.format(reKalibrasi.getTanggalKal()));
		DetailJenisExample ex = new DetailJenisExample();
		ex.createCriteria().andJenisEqualTo(reKalibrasi.getJenis());
		listDetailJenis = detailJenisMapper.selectByExample(ex);
		return SUCCESS;
	}
	
	public String addReKalibrasi(){
		System.out.println("Jalankan method addReKalibrasi");
		
		reKalibrasi.setTanggalKal(new Date());
		reKalibrasiMapper.insert(reKalibrasi);
		
		DetailJenis dj;
		if(hasilUkur1.length > 0){
			for (int i = 0; hasilUkur1.length > i; i++) {
				dj = new DetailJenis(); 
				dj.setJenis(reKalibrasi.getJenis());
				dj.setTanggalKal(reKalibrasi.getTanggalKal());
				dj.setHasilUkur1(hasilUkur1[i]);
				dj.setHasilUkur2(hasilUkur2[i]);
				dj.setHasilUkur3(hasilUkur3[i]);
				dj.setHasilUkur4(hasilUkur4[i]);
				dj.setHasilUkur5(hasilUkur5[i]);
				
				try {
					detailJenisMapper.insert(dj);
				} catch (Exception e) {
					addFieldError("", "gagal Menambahkan hasil ukur");
					return ERROR;
				}
			}
		}
		return SUCCESS;
	}
	
	public String editReKalibrasi(){
		System.out.println("Jalankan method editReKalibrasi");

		try {
			Date ex = sdf.parse(getStrDate());
			reKalibrasi.setTanggalKal(ex);
			reKalibrasiMapper.updateByPrimaryKey(reKalibrasi);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		DetailJenis dj;
		if(hasilUkur1.length > 0){
			
			DetailJenisExample exam = new DetailJenisExample();
			exam.createCriteria().andJenisEqualTo(reKalibrasi.getJenis()).andTanggalKalEqualTo(reKalibrasi.getTanggalKal());
			
			detailJenisMapper.deleteByExample(exam);
			for (int i = 0; hasilUkur1.length > i; i++) {
				dj = new DetailJenis(); 
				dj.setJenis(reKalibrasi.getJenis());
				dj.setTanggalKal(reKalibrasi.getTanggalKal());				
				dj.setHasilUkur1(hasilUkur1[i]);
				dj.setHasilUkur2(hasilUkur2[i]);
				dj.setHasilUkur3(hasilUkur3[i]);
				dj.setHasilUkur4(hasilUkur4[i]);
				dj.setHasilUkur5(hasilUkur5[i]);
				
				try {
					detailJenisMapper.insert(dj);
				} catch (Exception e) {
					addFieldError("", "gagal Menambahkan hasil ukur");
					return ERROR;
				}
			}
		}
		return SUCCESS;
	}
	
	public String deleteReKalibrasi(){
		System.out.println("Jalankan method deleteReKalibrasi");
		System.out.println("get jenis = "+reKalibrasi.getJenis());
		System.out.println("get jenis = "+reKalibrasi.getTanggalKal());
		try {
			Date dat = sdf.parse(getStrDate());
			DetailJenisExample ex = new DetailJenisExample();
			ex.createCriteria().andJenisEqualTo(reKalibrasi.getJenis()).andTanggalKalEqualTo(dat);
			detailJenisMapper.deleteByExample(ex);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		reKalibrasiMapper.deleteByPrimaryKey(reKalibrasi.getId());
		return SUCCESS;
	}
	
	public String reportReKalibrasi(){
		System.out.println("Jalankan method reportReKalibrasi");
		
		if(reKalibrasi == null){
			reKalibrasi = new ReKalibrasi();
			reKalibrasi.setJenis("");
		}
		
		listReKalibrasi = reKalibrasiMapper.selectToReport(reKalibrasi);
		return SUCCESS;
	}
	
	public String getReportReKalibrasi(){
		System.out.println("Jalankan method getReportReKalibrasi");
		
		reportReKalibrasi();
		File file=null;
        String filename="";        
        //get template
        InputStream inputStream=null;
        POIFSFileSystem fileSystem=null;
 	    reportFile = "rekalibrasi".concat(".xls");
 	    
 	    String d[] = sdf.format(new Date()).split("/"),date="";
		date+=d[0]+d[1]+d[2];
	    
		file = new File("C:/apache-tomcat-8.5.27/webapps"+"/"+"laporan_re_kalibrasi_"+date+".xls");
		System.out.println("file name"+file.getAbsolutePath());
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 	    
        //get Excel Template
		try {
			inputStream = new FileInputStream ("C:/apache-tomcat-8.5.27/webapps/"+reportFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}	
		
        try {
			fileSystem = new POIFSFileSystem (inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
      
        filename = file.getAbsolutePath();
    	HSSFWorkbook workbook=null;
		try {
			workbook = new HSSFWorkbook(fileSystem);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int rowNum=15;
		int cellNum=0;
		HSSFRow myRow = null;
		HSSFSheet reportReKalibrasi = workbook.getSheet("Sheet0");
		Row periode = reportReKalibrasi.createRow(12);
		periode.createCell(0).setCellValue("Jenis Kalibrasi : "+reKalibrasi.getJenis());
		Row header = reportReKalibrasi.createRow(14);
		header.createCell(cellNum++).setCellValue("Jenis");
		header.createCell(cellNum++).setCellValue("Tanggal Kal");
		header.createCell(cellNum++).setCellValue("Nama Alat");
		header.createCell(cellNum++).setCellValue("Nama Departemen");
		header.createCell(cellNum++).setCellValue("Nama Kalibrator");
		header.createCell(cellNum++).setCellValue("Range Resolusi");
		header.createCell(cellNum++).setCellValue("Penunjukan Kal");
		header.createCell(cellNum++).setCellValue("Hasil Ukur 1");
		header.createCell(cellNum++).setCellValue("Hasil Ukur 2");
		header.createCell(cellNum++).setCellValue("Hasil Ukur 3");
		header.createCell(cellNum++).setCellValue("Hasil Ukur 4");
		header.createCell(cellNum++).setCellValue("Hasil Ukur 5");
		header.createCell(cellNum++).setCellValue("X Rata");
		header.createCell(cellNum++).setCellValue("Penyimpangan");
		header.createCell(cellNum++).setCellValue("Kelembaban");
		header.createCell(cellNum++).setCellValue("Suhu");
		header.createCell(cellNum++).setCellValue("Hasil");
		header.createCell(cellNum++).setCellValue("Catatan");
		header.createCell(cellNum++).setCellValue("Pelaksana Kal");
		header.createCell(cellNum++).setCellValue("Kal Selanjutnya");

		for(ReKalibrasi rk : listReKalibrasi){
			int cell=0;
			myRow = reportReKalibrasi.createRow(rowNum++);
			//myRow.createCell(cell++).setCellValue(lp.getNamaBarang());
		}
		rowNum++;
		rowNum++;
		rowNum++;
		rowNum++;
		Row footer = reportReKalibrasi.createRow(rowNum++);
		/*footer.createCell(4).setCellValue(getWaktu());
		System.out.println("waktu pada footer"+getWaktu());*/
		rowNum++;
		rowNum++;
		rowNum++;
		Row ttd = reportReKalibrasi.createRow(rowNum++);
		ttd.createCell(4).setCellValue("Petugas Tata Usaha");
		try{
	        FileOutputStream fileOut =  new FileOutputStream(filename);
	        workbook.write(fileOut);
	        fileOut.close();
	        workbook.close();

	        File file2 = new File(filename);
	        this.inputStreamFile = new FileInputStream(file2);
			contentLength = file2.length();
			fileName = file2.getName();
       } catch ( Exception ex ) {
           System.out.println(ex);
       }
		return SUCCESS;
	}
	public String getStrDate() {
		return strDate;
	}
	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}
	public ReKalibrasi getReKalibrasi() {
		return reKalibrasi;
	}
	public void setReKalibrasi(ReKalibrasi reKalibrasi) {
		this.reKalibrasi = reKalibrasi;
	}
	public List<ReKalibrasi> getListReKalibrasi() {
		return listReKalibrasi;
	}
	public void setListReKalibrasi(List<ReKalibrasi> listReKalibrasi) {
		this.listReKalibrasi = listReKalibrasi;
	}
	public List<DetailJenis> getListDetailJenis() {
		return listDetailJenis;
	}
	public void setListDetailJenis(List<DetailJenis> listDetailJenis) {
		this.listDetailJenis = listDetailJenis;
	}
	public ReKalibrasiMapper getReKalibrasiMapper() {
		return reKalibrasiMapper;
	}
	public void setReKalibrasiMapper(ReKalibrasiMapper reKalibrasiMapper) {
		this.reKalibrasiMapper = reKalibrasiMapper;
	}
	public DetailJenisMapper getDetailJenisMapper() {
		return detailJenisMapper;
	}
	public void setDetailJenisMapper(DetailJenisMapper detailJenisMapper) {
		this.detailJenisMapper = detailJenisMapper;
	}
	public Integer[] getHasilUkur1() {
		return hasilUkur1;
	}
	public void setHasilUkur1(Integer[] hasilUkur1) {
		this.hasilUkur1 = hasilUkur1;
	}
	public Integer[] getHasilUkur2() {
		return hasilUkur2;
	}
	public void setHasilUkur2(Integer[] hasilUkur2) {
		this.hasilUkur2 = hasilUkur2;
	}
	public Integer[] getHasilUkur3() {
		return hasilUkur3;
	}
	public void setHasilUkur3(Integer[] hasilUkur3) {
		this.hasilUkur3 = hasilUkur3;
	}
	public Integer[] getHasilUkur4() {
		return hasilUkur4;
	}
	public void setHasilUkur4(Integer[] hasilUkur4) {
		this.hasilUkur4 = hasilUkur4;
	}
	public Integer[] getHasilUkur5() {
		return hasilUkur5;
	}
	public void setHasilUkur5(Integer[] hasilUkur5) {
		this.hasilUkur5 = hasilUkur5;
	}
	public InputStream getInputStreamFile() {
		return inputStreamFile;
	}
	public void setInputStreamFile(InputStream inputStreamFile) {
		this.inputStreamFile = inputStreamFile;
	}
	public String getReportFile() {
		return reportFile;
	}
	public void setReportFile(String reportFile) {
		this.reportFile = reportFile;
	}
	public long getContentLength() {
		return contentLength;
	}
	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
