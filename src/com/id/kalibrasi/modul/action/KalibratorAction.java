package com.id.kalibrasi.modul.action;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.id.kalibrasi.core.action.CoreAction;
import com.id.kalibrasi.modul.mapper.KalibratorMapper;
import com.id.kalibrasi.modul.model.Kalibrator;
import com.id.kalibrasi.modul.model.KalibratorExample;

public class KalibratorAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private String strDate;
	private Kalibrator kalibrator;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	private List<Kalibrator> listKalibrator = new ArrayList<>();
	private KalibratorMapper kalibratorMapper = (KalibratorMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("kalibratorMapper");
	
	public String searchAllKalibrator(){
		System.out.println("Jalankan method SearchAllKalibrator");
		
		if(kalibrator == null){
			kalibrator = new Kalibrator();
			kalibrator.setNamaKalibrator("");
		}
		
		KalibratorExample ex = new KalibratorExample();
		ex.createCriteria().andNamaKalibratorLike("%"+kalibrator.getNamaKalibrator()+"%");
		listKalibrator = kalibratorMapper.selectByExample(ex);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("Jalankan method SearchById");
		
		kalibrator = kalibratorMapper.selectByPrimaryKey(kalibrator.getKodeKalibrator());
		setStrDate(sdf.format(kalibrator.getExpDate()));
		return SUCCESS;
	}
	
	public String searchList(){
		System.out.println("Jalankan method searchList");
		
		KalibratorExample examp = new KalibratorExample();
		listKalibrator = kalibratorMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchByName(){
		System.out.println("Jalankan method searchByName");
		
		kalibrator = kalibratorMapper.selectByName(kalibrator);
		return SUCCESS;
	}
	
	public String saveKalibrator(){
		System.out.println("Jalankan method SaveKalibrator");
		
		try {
			Date ex = sdf.parse(getStrDate());
			kalibrator.setExpDate(ex);
			kalibratorMapper.insert(kalibrator);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String editKalibrator(){
		System.out.println("Jalankan method editKalibrator");
		
		try {
			Date ex = sdf.parse(getStrDate());
			kalibrator.setExpDate(ex);
			kalibratorMapper.updateByPrimaryKey(kalibrator);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String deleteKalibrator(){
		System.out.println("Jalankan method deleteKalibrator");
		
		kalibratorMapper.deleteByPrimaryKey(kalibrator.getKodeKalibrator());
		return SUCCESS;
	}
	
	public String getStrDate() {
		return strDate;
	}
	public void setStrDate(String strDate) {
		this.strDate = strDate;
	}	
	public Kalibrator getKalibrator() {
		return kalibrator;
	}
	public void setKalibrator(Kalibrator kalibrator) {
		this.kalibrator = kalibrator;
	}
	public List<Kalibrator> getListKalibrator() {
		return listKalibrator;
	}
	public void setListKalibrator(List<Kalibrator> listKalibrator) {
		this.listKalibrator = listKalibrator;
	}
	public KalibratorMapper getKalibratorMapper() {
		return kalibratorMapper;
	}
	public void setKalibratorMapper(KalibratorMapper kalibratorMapper) {
		this.kalibratorMapper = kalibratorMapper;
	}
}
