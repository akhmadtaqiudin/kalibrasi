package com.id.kalibrasi.modul.action;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.id.kalibrasi.core.action.CoreAction;
import com.id.kalibrasi.modul.mapper.DepartementMapper;
import com.id.kalibrasi.modul.model.Departement;
import com.id.kalibrasi.modul.model.DepartementExample;

public class DepartementAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private Departement departement;
	private List<Departement> listDepartement = new ArrayList<>();
	private DepartementMapper departementMapper = (DepartementMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("departementMapper");
	
	public String searchAllDepartement(){
		System.out.println("Jalankan method SearchAllDepartement");
		
		if(departement == null){
			departement = new Departement();
			departement.setNamaDept("");
		}
		
		DepartementExample examp = new DepartementExample();
		examp.createCriteria().andNamaDeptLike("%"+departement.getNamaDept()+"%");
		
		listDepartement = departementMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("Jalankan method SearchById");
		
		departement = departementMapper.selectByPrimaryKey(departement.getIdDept());
		return SUCCESS;
	}
	
	public String searchList(){
		System.out.println("Jalankan method searchList");
		
		DepartementExample examp = new DepartementExample();
		listDepartement = departementMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchByName(){
		System.out.println("Jalankan method searchByName");
		
		departement = departementMapper.selectByName(departement);
		return SUCCESS;
	}
	
	public String saveDepartement(){
		System.out.println("Jalankan method SaveDepartement");
		
		DepartementExample examp = new DepartementExample();
		examp.createCriteria().andNamaDeptEqualTo(departement.getNamaDept());
		
		long x = departementMapper.countByExample(examp);
		if(x > 0){
			addFieldError("invalidDept", "Maaf Nama Departement = "+departement.getNamaDept()+" sudah terdaftar pada database");
			return ERROR;
		}
		departementMapper.insert(departement);
		return SUCCESS;
	}
	
	public String editDepartement(){
		System.out.println("Jalankan method editDepartement");
		
		departementMapper.updateByPrimaryKey(departement);
		return SUCCESS;
	}
	
	public String deleteDepartement(){
		System.out.println("Jalankan method deleteDepartement");
		
		departementMapper.deleteByPrimaryKey(departement.getIdDept());
		return SUCCESS;
	}
	
	public Departement getDepartement() {
		return departement;
	}
	public void setDepartement(Departement departement) {
		this.departement = departement;
	}
	public List<Departement> getListDepartement() {
		return listDepartement;
	}
	public void setListDepartement(List<Departement> listDepartement) {
		this.listDepartement = listDepartement;
	}
	public DepartementMapper getDepartementMapper() {
		return departementMapper;
	}
	public void setDepartementMapper(DepartementMapper departementMapper) {
		this.departementMapper = departementMapper;
	}
}
