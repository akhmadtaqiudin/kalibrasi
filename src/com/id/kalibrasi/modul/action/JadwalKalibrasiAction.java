package com.id.kalibrasi.modul.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.id.kalibrasi.core.action.CoreAction;
import com.id.kalibrasi.modul.mapper.JadwalKalibrasiMapper;
import com.id.kalibrasi.modul.model.JadwalKalibrasi;

public class JadwalKalibrasiAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private String strEstimasi;
	private JadwalKalibrasi jadwalKalibrasi;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	private InputStream inputStreamFile;
	private String reportFile;
	private long contentLength;
	private String fileName;
	
	private List<JadwalKalibrasi> listJadwal = new ArrayList<>();
	private JadwalKalibrasiMapper jadwalKalibrasiMapper = (JadwalKalibrasiMapper) new ClassPathXmlApplicationContext("config-db.xml").getBean("jadwalKalibrasiMapper");
	
	public String searchAllJadwalKalibrasi(){
		System.out.println("Jalankan method SearchAllJadwalKalibrasi");
		
		if(jadwalKalibrasi == null){
			jadwalKalibrasi = new JadwalKalibrasi();
			jadwalKalibrasi.setNamaAlat("");
		}
		listJadwal = jadwalKalibrasiMapper.customSelect(jadwalKalibrasi);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("Jalankan method SearchById");
		
		jadwalKalibrasi = jadwalKalibrasiMapper.selectByPrimaryKey(jadwalKalibrasi.getIdJadwal());
		setStrEstimasi(sdf.format(jadwalKalibrasi.getEstimasiKal()));
		return SUCCESS;
	}
	
	public String saveJadwalKalibrasi(){
		System.out.println("Jalankan method SaveJadwalKalibrasi");
		
		try {
			Date ex2 = sdf.parse(getStrEstimasi());
			jadwalKalibrasi.setEstimasiKal(ex2);
			jadwalKalibrasiMapper.insert(jadwalKalibrasi);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String editJadwalKalibrasi(){
		System.out.println("Jalankan method editJadwalKalibrasi");		
		
		try {
			Date ex = sdf.parse(getStrEstimasi());
			jadwalKalibrasi.setEstimasiKal(ex);
			jadwalKalibrasiMapper.updateByPrimaryKey(jadwalKalibrasi);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String deleteJadwalKalibrasi(){
		System.out.println("Jalankan method deleteJadwalKalibrasi");
		
		jadwalKalibrasiMapper.deleteByPrimaryKey(jadwalKalibrasi.getIdJadwal());
		return SUCCESS;
	}
	
	public String getReportJadwal(){
		System.out.println("");
		
		searchAllJadwalKalibrasi();
		File file=null;
        String filename="";        
        //get template
        InputStream inputStream=null;
        POIFSFileSystem fileSystem=null;
 	    reportFile = "jadwal".concat(".xls");
 	    
 	    String d[] = sdf.format(new Date()).split("/"),date="";
		date+=d[0]+d[1]+d[2];
	    
		file = new File("C:/apache-tomcat-8.5.27/webapps"+"/"+"laporan_jadwal_"+date+".xls");
		System.out.println("file name"+file.getAbsolutePath());
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 	    
       //get Excel Template
		try {
			inputStream = new FileInputStream ("C:/apache-tomcat-8.5.27/webapps/"+reportFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}	
		
        try {
			fileSystem = new POIFSFileSystem (inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
     
        filename = file.getAbsolutePath();
   	    HSSFWorkbook workbook=null;
		try {
			workbook = new HSSFWorkbook(fileSystem);
		} catch (IOException e) {
			e.printStackTrace();
		}
 	    return SUCCESS;
	}
	public String getStrEstimasi() {
		return strEstimasi;
	}
	public void setStrEstimasi(String strEstimasi) {
		this.strEstimasi = strEstimasi;
	}
	public JadwalKalibrasi getJadwalKalibrasi() {
		return jadwalKalibrasi;
	}
	public void setJadwalKalibrasi(JadwalKalibrasi jadwalKalibrasi) {
		this.jadwalKalibrasi = jadwalKalibrasi;
	}
	public List<JadwalKalibrasi> getListJadwal() {
		return listJadwal;
	}
	public void setListJadwal(List<JadwalKalibrasi> listJadwal) {
		this.listJadwal = listJadwal;
	}
	public JadwalKalibrasiMapper getJadwalKalibrasiMapper() {
		return jadwalKalibrasiMapper;
	}
	public void setJadwalKalibrasiMapper(JadwalKalibrasiMapper jadwalKalibrasiMapper) {
		this.jadwalKalibrasiMapper = jadwalKalibrasiMapper;
	}
	public InputStream getInputStreamFile() {
		return inputStreamFile;
	}
	public void setInputStreamFile(InputStream inputStreamFile) {
		this.inputStreamFile = inputStreamFile;
	}
	public String getReportFile() {
		return reportFile;
	}
	public void setReportFile(String reportFile) {
		this.reportFile = reportFile;
	}
	public long getContentLength() {
		return contentLength;
	}
	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}