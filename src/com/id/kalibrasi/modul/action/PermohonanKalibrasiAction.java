package com.id.kalibrasi.modul.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.id.kalibrasi.core.action.CoreAction;
import com.id.kalibrasi.modul.mapper.PermohonanKalibrasiMapper;
import com.id.kalibrasi.modul.model.PermohonanKalibrasi;
import com.id.kalibrasi.modul.model.PermohonanKalibrasiExample;

public class PermohonanKalibrasiAction extends CoreAction{

	private static final long serialVersionUID = 1L;
	private String strAwal;
	private String strAkhir;
	private PermohonanKalibrasi permohonan;
	SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	private InputStream inputStreamFile;
	private String reportFile;
	private long contentLength;
	private String fileName;
	
	private List<PermohonanKalibrasi> listPermohonan = new ArrayList<>();
	private PermohonanKalibrasiMapper permohonanKalibrasiMapper = (PermohonanKalibrasiMapper) 
			new ClassPathXmlApplicationContext("config-db.xml").getBean("permohonanKalibrasiMapper");
	
	public String searchAllPermohonan(){
		System.out.println("Jalankan method SearchAllPermohonan");
		
		if(permohonan == null){
			permohonan = new PermohonanKalibrasi();
			permohonan.setNamaAlat("");
		}
		
		PermohonanKalibrasiExample examp = new PermohonanKalibrasiExample();
		examp.createCriteria().andNamaAlatLike("%"+permohonan.getNamaAlat()+"%");
		listPermohonan = permohonanKalibrasiMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchToDashboard(){
		System.out.println("Jalankan method searchToDashboard");
		
		if(permohonan == null){
			permohonan = new PermohonanKalibrasi();
			permohonan.setStatusPermohonan("Process");
		}
		
		PermohonanKalibrasiExample examp = new PermohonanKalibrasiExample();
		examp.createCriteria().andStatusPermohonanEqualTo(permohonan.getStatusPermohonan());
		listPermohonan = permohonanKalibrasiMapper.selectByExample(examp);
		return SUCCESS;
	}
	
	public String searchById(){
		System.out.println("Jalankan method SearchById");
		
		permohonan = permohonanKalibrasiMapper.selectByPrimaryKey(permohonan.getNoPermohonan());
		setStrAwal(sdf.format(permohonan.getExpAwal()));
		setStrAkhir(sdf.format(permohonan.getExpAkhir()));
		return SUCCESS;
	}
	
	public String savePermohonan(){
		System.out.println("Jalankan method SavePermohonan");
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd");
		String d[] = sdf2.format(new Date()).split("/"),kode="";
		kode+=d[0]+d[1]+d[2];
		
		String no = "";
		no = permohonanKalibrasiMapper.selectMaxId(permohonan);
		if(no==null){
			no = "0";
		}else{
			no = no.substring(8);
		}
		
		int x = Integer.parseInt(no);
		x++;
		if(x <99 && x>=10){
			kode+="00"+x;
		}else if(x>99 && x<1000){
			kode+="0"+x;
		}else if(x>=999){
			kode+=x;
		}else{
			kode+="000"+x;
		}
		
		try {
			Date ex1 = sdf.parse(getStrAwal());
			Date ex2 = sdf.parse(getStrAkhir());
			
			permohonan.setNoPermohonan(kode);
			permohonan.setExpAwal(ex1);
			permohonan.setExpAkhir(ex2);
			permohonan.setTglPermohonan(new Date());
			permohonanKalibrasiMapper.insert(permohonan);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return SUCCESS;
	}
	
	public String editPermohonan(){
		System.out.println("Jalankan method editPermohonan");
		
		try {
			Date ex1 = sdf.parse(getStrAwal());
			Date ex2 = sdf.parse(getStrAkhir());
			
			permohonan.setExpAwal(ex1);
			permohonan.setExpAkhir(ex2);
			permohonanKalibrasiMapper.updateByPrimaryKey(permohonan);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String deletePermohonan(){
		System.out.println("Jalankan method deletePermohonan");
		
		permohonanKalibrasiMapper.deleteByPrimaryKey(permohonan.getNoPermohonan());
		return SUCCESS;
	}
	
	public String reportPermohonan(){
		System.out.println("jalankan method reportPermohonan");
			

		PermohonanKalibrasiExample exam = new PermohonanKalibrasiExample();
		try {
			if(strAwal == null || strAwal.equalsIgnoreCase("") && strAkhir == null || strAkhir.equalsIgnoreCase("")){
				exam.createCriteria().andTglPermohonanBetween(new Date(), new Date());
				listPermohonan = permohonanKalibrasiMapper.selectByExample(exam);
			}else{
				exam.createCriteria().andTglPermohonanBetween(sdf.parse(getStrAwal()), sdf.parse(getStrAkhir()));
				listPermohonan = permohonanKalibrasiMapper.selectByExample(exam);
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return SUCCESS;
	}
	
	public String getReportPermohonan(){
		System.out.println("");
		
		reportPermohonan();
		File file=null;
        String filename="";        
        //get template
        InputStream inputStream=null;
        POIFSFileSystem fileSystem=null;
 	    reportFile = "permohonan".concat(".xls");
 	    
 	    String d[] = sdf.format(new Date()).split("/"),date="";
		date+=d[0]+d[1]+d[2];
	    
		file = new File("C:/apache-tomcat-8.5.27/webapps"+"/"+"laporan_permohonan_"+date+".xls");
		System.out.println("file name"+file.getAbsolutePath());
		if(!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} 	    
        //get Excel Template
		try {
			inputStream = new FileInputStream ("C:/apache-tomcat-8.5.27/webapps/"+reportFile);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}	
		
        try {
			fileSystem = new POIFSFileSystem (inputStream);
		} catch (IOException e) {
			e.printStackTrace();
		}
     
        filename = file.getAbsolutePath();
   	    HSSFWorkbook workbook=null;
		try {
			workbook = new HSSFWorkbook(fileSystem);
		} catch (IOException e) {
			e.printStackTrace();
		}		
		
		int rowNum=15;
		int cellNum=0;
		HSSFRow myRow = null;
		HSSFSheet reportPermohonan = workbook.getSheet("Sheet0");
		Row periode = reportPermohonan.createRow(12);
		periode.createCell(0).setCellValue("Periode : "+sdf.format(getStrAwal())+" -");
		periode.createCell(1).setCellValue(sdf.format(getStrAkhir()));
		Row header = reportPermohonan.createRow(14);
		header.createCell(cellNum++).setCellValue("No Permohonan");
		header.createCell(cellNum++).setCellValue("No Penerimaan");
		header.createCell(cellNum++).setCellValue("Nama Alat");
		header.createCell(cellNum++).setCellValue("Tanggal Permohonan");
		header.createCell(cellNum++).setCellValue("Expired Awal");
		header.createCell(cellNum++).setCellValue("Expired Akhir");
		header.createCell(cellNum++).setCellValue("Tempat Pelaksanaan");
		header.createCell(cellNum++).setCellValue("Status Permohonan");
		header.createCell(cellNum++).setCellValue("Catatan");
		
		for(PermohonanKalibrasi pk : listPermohonan){
			int cell=0;
			myRow = reportPermohonan.createRow(rowNum++);
			myRow.createCell(cell++).setCellValue(pk.getNoPermohonan());
			myRow.createCell(cell++).setCellValue(pk.getNoPenerimaan());
			myRow.createCell(cell++).setCellValue(pk.getNamaAlat());
			myRow.createCell(cell++).setCellValue(sdf.format(pk.getTglPermohonan()));
			myRow.createCell(cell++).setCellValue(sdf.format(pk.getExpAwal()));
			myRow.createCell(cell++).setCellValue(sdf.format(pk.getExpAkhir()));
			myRow.createCell(cell++).setCellValue(pk.getTempatPelaksanaan());
			myRow.createCell(cell++).setCellValue(pk.getStatusPermohonan());
			myRow.createCell(cell++).setCellValue(pk.getCatatan());
		}
		try{
	        FileOutputStream fileOut =  new FileOutputStream(filename);
	        workbook.write(fileOut);
	        fileOut.close();
	        workbook.close();

	        File file2 = new File(filename);
	        this.inputStreamFile = new FileInputStream(file2);
			contentLength = file2.length();
			fileName = file2.getName();
       } catch ( Exception ex ) {
           System.out.println(ex);
       }
		return SUCCESS;
	}
	public String getStrAwal() {
		return strAwal;
	}
	public void setStrAwal(String strAwal) {
		this.strAwal = strAwal;
	}
	public String getStrAkhir() {
		return strAkhir;
	}
	public void setStrAkhir(String strAkhir) {
		this.strAkhir = strAkhir;
	}
	public PermohonanKalibrasi getPermohonan() {
		return permohonan;
	}
	public void setPermohonan(PermohonanKalibrasi permohonan) {
		this.permohonan = permohonan;
	}
	public List<PermohonanKalibrasi> getListPermohonan() {
		return listPermohonan;
	}
	public void setListPermohonan(List<PermohonanKalibrasi> listPermohonan) {
		this.listPermohonan = listPermohonan;
	}
	public PermohonanKalibrasiMapper getPermohonanKalibrasiMapper() {
		return permohonanKalibrasiMapper;
	}
	public void setPermohonanKalibrasiMapper(PermohonanKalibrasiMapper permohonanKalibrasiMapper) {
		this.permohonanKalibrasiMapper = permohonanKalibrasiMapper;
	}
	public InputStream getInputStreamFile() {
		return inputStreamFile;
	}
	public void setInputStreamFile(InputStream inputStreamFile) {
		this.inputStreamFile = inputStreamFile;
	}
	public String getReportFile() {
		return reportFile;
	}
	public void setReportFile(String reportFile) {
		this.reportFile = reportFile;
	}
	public long getContentLength() {
		return contentLength;
	}
	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}