package com.id.kalibrasi.modul.mapper;

import com.id.kalibrasi.modul.model.JadwalKalibrasi;
import com.id.kalibrasi.modul.model.JadwalKalibrasiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface JadwalKalibrasiMapper {
    long countByExample(JadwalKalibrasiExample example);

    int deleteByExample(JadwalKalibrasiExample example);

    int deleteByPrimaryKey(Integer idJadwal);

    int insert(JadwalKalibrasi record);

    int insertSelective(JadwalKalibrasi record);

    List<JadwalKalibrasi> selectByExample(JadwalKalibrasiExample example);
    
    List<JadwalKalibrasi> customSelect(JadwalKalibrasi record);

    JadwalKalibrasi selectByPrimaryKey(Integer idJadwal);

    int updateByExampleSelective(@Param("record") JadwalKalibrasi record, @Param("example") JadwalKalibrasiExample example);

    int updateByExample(@Param("record") JadwalKalibrasi record, @Param("example") JadwalKalibrasiExample example);

    int updateByPrimaryKeySelective(JadwalKalibrasi record);

    int updateByPrimaryKey(JadwalKalibrasi record);
}