package com.id.kalibrasi.modul.mapper;

import java.util.List;
import com.id.kalibrasi.modul.model.Departement;
import com.id.kalibrasi.modul.model.DepartementExample;
import org.apache.ibatis.annotations.Param;

public interface DepartementMapper {
    long countByExample(DepartementExample example);

    int deleteByExample(DepartementExample example);

    int deleteByPrimaryKey(Integer idDept);

    int insert(Departement record);

    int insertSelective(Departement record);

    List<Departement> selectByExample(DepartementExample example);

    Departement selectByPrimaryKey(Integer idDept);
    
    Departement selectByName(Departement record);

    int updateByExampleSelective(@Param("record") Departement record, @Param("example") DepartementExample example);

    int updateByExample(@Param("record") Departement record, @Param("example") DepartementExample example);

    int updateByPrimaryKeySelective(Departement record);

    int updateByPrimaryKey(Departement record);
}