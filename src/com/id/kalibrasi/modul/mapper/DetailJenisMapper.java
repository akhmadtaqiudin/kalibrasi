package com.id.kalibrasi.modul.mapper;

import com.id.kalibrasi.modul.model.DetailJenis;
import com.id.kalibrasi.modul.model.DetailJenisExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface DetailJenisMapper {
    long countByExample(DetailJenisExample example);

    int deleteByExample(DetailJenisExample example);

    int deleteByPrimaryKey(Integer idDetail);

    int insert(DetailJenis record);

    int insertSelective(DetailJenis record);

    List<DetailJenis> selectByExample(DetailJenisExample example);

    DetailJenis selectByPrimaryKey(Integer idDetail);

    int updateByExampleSelective(@Param("record") DetailJenis record, @Param("example") DetailJenisExample example);

    int updateByExample(@Param("record") DetailJenis record, @Param("example") DetailJenisExample example);

    int updateByPrimaryKeySelective(DetailJenis record);

    int updateByPrimaryKey(DetailJenis record);
}