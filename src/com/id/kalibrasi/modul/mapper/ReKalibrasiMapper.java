package com.id.kalibrasi.modul.mapper;

import com.id.kalibrasi.modul.model.ReKalibrasi;
import com.id.kalibrasi.modul.model.ReKalibrasiExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface ReKalibrasiMapper {
    long countByExample(ReKalibrasiExample example);

    int deleteByExample(ReKalibrasiExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(ReKalibrasi record);

    int insertSelective(ReKalibrasi record);

    List<ReKalibrasi> selectByExample(ReKalibrasiExample example);
    
    List<ReKalibrasi> selectToReport(ReKalibrasi record);

    ReKalibrasi selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") ReKalibrasi record, @Param("example") ReKalibrasiExample example);

    int updateByExample(@Param("record") ReKalibrasi record, @Param("example") ReKalibrasiExample example);

    int updateByPrimaryKeySelective(ReKalibrasi record);

    int updateByPrimaryKey(ReKalibrasi record);
}