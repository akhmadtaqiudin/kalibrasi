package com.id.kalibrasi.modul.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.id.kalibrasi.modul.model.Kalibrator;
import com.id.kalibrasi.modul.model.KalibratorExample;

public interface KalibratorMapper {
    long countByExample(KalibratorExample example);

    int deleteByExample(KalibratorExample example);

    int deleteByPrimaryKey(Integer kodeKalibrator);

    int insert(Kalibrator record);

    int insertSelective(Kalibrator record);

    List<Kalibrator> selectByExample(KalibratorExample example);

    Kalibrator selectByPrimaryKey(Integer kodeKalibrator);
    
    Kalibrator selectByName(Kalibrator record);

    int updateByExampleSelective(@Param("record") Kalibrator record, @Param("example") KalibratorExample example);

    int updateByExample(@Param("record") Kalibrator record, @Param("example") KalibratorExample example);

    int updateByPrimaryKeySelective(Kalibrator record);

    int updateByPrimaryKey(Kalibrator record);
}