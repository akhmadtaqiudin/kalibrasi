package com.id.kalibrasi.modul.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.id.kalibrasi.modul.model.PermohonanKalibrasi;
import com.id.kalibrasi.modul.model.PermohonanKalibrasiExample;

public interface PermohonanKalibrasiMapper {
    long countByExample(PermohonanKalibrasiExample example);

    int deleteByExample(PermohonanKalibrasiExample example);

    int deleteByPrimaryKey(String noPermohonan);

    int insert(PermohonanKalibrasi record);

    int insertSelective(PermohonanKalibrasi record);
    
    String selectMaxId(PermohonanKalibrasi record); 

    List<PermohonanKalibrasi> selectByExample(PermohonanKalibrasiExample example);

    PermohonanKalibrasi selectByPrimaryKey(String noPermohonan);

    int updateByExampleSelective(@Param("record") PermohonanKalibrasi record, @Param("example") PermohonanKalibrasiExample example);

    int updateByExample(@Param("record") PermohonanKalibrasi record, @Param("example") PermohonanKalibrasiExample example);

    int updateByPrimaryKeySelective(PermohonanKalibrasi record);

    int updateByPrimaryKey(PermohonanKalibrasi record);
}