package com.id.kalibrasi.modul.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.id.kalibrasi.modul.model.AlatUkur;
import com.id.kalibrasi.modul.model.AlatUkurExample;


public interface AlatUkurMapper {
    long countByExample(AlatUkurExample example);

    int deleteByExample(AlatUkurExample example);

    int deleteByPrimaryKey(String kodeAlat);

    int insert(AlatUkur record);

    int insertSelective(AlatUkur record);
    
    String selectMaxId(AlatUkur record);

    List<AlatUkur> selectByExample(AlatUkurExample example);

    AlatUkur selectByPrimaryKey(String kodeAlat);
    
    AlatUkur selectByName(AlatUkur record);

    int updateByExampleSelective(@Param("record") AlatUkur record, @Param("example") AlatUkurExample example);

    int updateByExample(@Param("record") AlatUkur record, @Param("example") AlatUkurExample example);

    int updateByPrimaryKeySelective(AlatUkur record);

    int updateByPrimaryKey(AlatUkur record);
}